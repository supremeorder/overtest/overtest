﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectronNET.API;
using ElectronNET.API.Entities;
using Newtonsoft.Json;
using Overtest.SharedLibraries.ProgrammingTasksShared;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;

namespace OTaskForce.Singletons
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class ProjectsOperator
    {
        public const string DefaultProjectsDirectoryName = "OTaskForce";
        
        public const string ProjectConfigurationFileName = ".otaskforceproj";
        public const string TaskDescriptionFileName = "description.html";
        
        public const string VerificationDataDirectoryName = "verificationdata";
        public const string VerificationDataConfigurationFileName = "verificationdata.config.json";
        public const int VerificationDataSchemaVersion = 1;

        public string DefaultProjectsDirectoryFullPath { get; }
        public RecentProjectsOperator RecentProjectsControl { get; }
        public CurrentProjectOperator CurrentProjectControl { get; }
        
        public ProjectsOperator()
        {
            DefaultProjectsDirectoryFullPath = Path.Combine(
                Electron.App.GetPathAsync(PathName.Documents).Result,
                DefaultProjectsDirectoryName
            );
            
            if (!Directory.Exists(DefaultProjectsDirectoryFullPath))
            {
                Directory.CreateDirectory(DefaultProjectsDirectoryFullPath);
                new DirectoryInfo(DefaultProjectsDirectoryFullPath).Attributes &= ~FileAttributes.ReadOnly;
            }
            
            RecentProjectsControl = new RecentProjectsOperator(DefaultProjectsDirectoryFullPath);
            CurrentProjectControl = new CurrentProjectOperator(RecentProjectsControl);
        }

        public async Task CreateNewProjectAsync(string path, string name)
        {
            /*
             * Basic project files setup
             */
            
            var projectDirectoryFullPath = Path.Combine(path, name);
            
            // Throw an exception if specified directory is already exists
            if (Directory.Exists(projectDirectoryFullPath))
                throw new AccessViolationException();
            
            // Create projects directory
            _ = Directory.CreateDirectory(projectDirectoryFullPath);
            
            // Create project's configuration file
            await File.WriteAllTextAsync(
                Path.Combine(projectDirectoryFullPath, ProjectConfigurationFileName),
                JsonConvert.SerializeObject(GetNewProjectProperties()), Encoding.UTF8
            );
            
            /*
             * Task description initialization
             */
            await File.WriteAllTextAsync(Path.Combine(projectDirectoryFullPath, TaskDescriptionFileName),
                "Task description goes here...", Encoding.UTF8);
            
            /*
             * Verification data setup
             */
            
            var verificationDataDirectoryFullPath = Path.Combine(projectDirectoryFullPath, VerificationDataDirectoryName);
            
            // Create verification data directory
            _ = Directory.CreateDirectory(verificationDataDirectoryFullPath);
            
            // Create verification data configuration file
            await File.WriteAllTextAsync(
                Path.Combine(verificationDataDirectoryFullPath, VerificationDataConfigurationFileName),
                JsonConvert.SerializeObject(GetTypicalVerificationDataConfiguration(), Formatting.Indented),
                Encoding.UTF8
            );
        }

        private TaskForceProjectPropertiesStore GetNewProjectProperties()
        {
            return new()
            {
                ProgrammingTaskId = Guid.NewGuid().ToString()
            };
        }
        
        private VerificationDataConfiguration GetTypicalVerificationDataConfiguration()
        {
            return new ()
            {
                SchemaVersion = VerificationDataSchemaVersion,
                
                AuthorSolution = new AuthorSolutionStore
                {
                    CompilerId = null,
                    SourceCodePath = "author_solution"
                },
                
                TestLib = new TestLibConfiguration
                {
                    InitializerPath = null,
                    FinalizerPath = null,
                    
                    GeneratorPath = null,
                    CheckerPath = null,
                    
                    DefaultInputFileName = "input.dat",
                    DefaultOutputFileName = "output.dat"
                },
                
                RuntimeLimits = new RuntimeLimitsStore { MaxRealTime = 2000, MaxProcessorTime = 1000, PeakWorkingSet = 104857600 },
                
                ReleaseConfiguration = new ReleaseModeConfiguration
                {
                    RealTimeKillsLimit = 1,
                    Tests = new List<SingleTestInfo>
                    {
                        new () { Title = "Test #1", RuntimeLimits = null }
                    }
                }
            };
        }
    }

    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public sealed class CurrentProjectOperator
    {
        private readonly RecentProjectsOperator _recentProjectsOperator;
        
        public bool HasProjectOpened { get; private set; }
        public string ProjectDirectoryFullPath { get; private set; }
        public string ProjectName { get; private set; }

        public string ProjectFileFullPath => HasProjectOpened
            ? Path.Combine(ProjectDirectoryFullPath, ProjectsOperator.ProjectConfigurationFileName)
            : null;

        public string TaskDescriptionFullPath => HasProjectOpened
            ? Path.Combine(ProjectDirectoryFullPath, ProjectsOperator.TaskDescriptionFileName)
            : null;

        public CurrentProjectOperator(RecentProjectsOperator recentProjectsOperator)
        {
            _recentProjectsOperator = recentProjectsOperator;
            
            HasProjectOpened = false;
            ProjectDirectoryFullPath = ProjectName = null;
        }
        
        public async Task<bool> TryOpenProjectAsync(string fullPath)
        {
            try
            {
                if (HasProjectOpened)
                    CloseProject();
                
                var projectDirectoryInfo = new DirectoryInfo(fullPath);
                
                // Verify specified project's directory path exists
                if (!projectDirectoryInfo.Exists)
                    return false;
                
                // Verify project's configuration file exists
                if (!File.Exists(Path.Combine(fullPath, ProjectsOperator.ProjectConfigurationFileName)))
                    return false;
                
                // Open project in OTaskForce
                HasProjectOpened = true;
                ProjectName = projectDirectoryInfo.Name;
                ProjectDirectoryFullPath = fullPath;
                
                // Update last access time of the project's directory
                projectDirectoryInfo.LastAccessTime = DateTime.Now;
                
                // Add current project to 'Recent projects' list
                await _recentProjectsOperator.AddProjectToRecentsAsync(ProjectDirectoryFullPath);
                
                return true;
            }
            catch
            {
                HasProjectOpened = false;
                ProjectName = ProjectDirectoryFullPath = null;
                return false;
            }
        }

        public bool CloseProject()
        {
            if (!HasProjectOpened)
                return false;

            HasProjectOpened = false;
            ProjectName = ProjectDirectoryFullPath = null;

            return true;
        }
        
        #region VerificationData operations
        
        public string VerificationDataDirectoryPath => HasProjectOpened
            ? Path.Combine(ProjectDirectoryFullPath, ProjectsOperator.VerificationDataDirectoryName)
            : null;

        public string VerificationDataConfigurationFilePath => HasProjectOpened
            ? Path.Combine(VerificationDataDirectoryPath, ProjectsOperator.VerificationDataConfigurationFileName)
            : null;

        public async Task<VerificationDataConfiguration> GetVerificationDataConfigurationAsync()
        {
            if (!HasProjectOpened)
                return null;
            
            if (!File.Exists(VerificationDataConfigurationFilePath))
                throw new FileNotFoundException(null, VerificationDataConfigurationFilePath);

            try
            {
                var fileContent = await File.ReadAllTextAsync(VerificationDataConfigurationFilePath, Encoding.UTF8);
                return JsonConvert.DeserializeObject<VerificationDataConfiguration>(fileContent);
            }
            catch (Exception ex) { throw new FileLoadException(null, VerificationDataConfigurationFilePath, ex); }
        }

        public async Task SetVerificationDataConfigurationAsync(VerificationDataConfiguration newConfiguration)
        {
            if (!HasProjectOpened)
                throw new InvalidOperationException();

            try
            {
                var backupFIlePath = VerificationDataConfigurationFilePath + ".old";
                
                if (File.Exists(backupFIlePath))
                    File.Delete(backupFIlePath);
                
                File.Copy(VerificationDataConfigurationFilePath, backupFIlePath);
                
                await File.WriteAllTextAsync(VerificationDataConfigurationFilePath,
                    JsonConvert.SerializeObject(newConfiguration, Formatting.Indented), Encoding.UTF8);
            }
            catch (Exception ex) { throw new AccessViolationException(null, ex); }
        }
        
        #endregion
        
    }
    
    public sealed class RecentProjectsOperator
    {
        private const string RecentProjectsListFileName = "recent_projects.json";
        
        private readonly string _projectsDirectoryPath;

        public RecentProjectsOperator(string projectsDirectoryPath)
        {
            _projectsDirectoryPath = projectsDirectoryPath;
        }
        
        private string GetRecentProjectsFilePathAsync() =>
            Path.Combine(_projectsDirectoryPath, RecentProjectsListFileName);
        
        public async Task AddProjectToRecentsAsync(string recentProjectDirectoryPath)
        {
            var recentProjectsListFilePath = GetRecentProjectsFilePathAsync();
            var recentProjectsList = await GetRecentProjectsListAsync();
            
            recentProjectsList.Remove(recentProjectDirectoryPath);

            if (recentProjectsList.Count > 4)
                recentProjectsList = recentProjectsList.Take(4).ToList();

            recentProjectsList = recentProjectsList.Prepend(recentProjectDirectoryPath).ToList();

            await File.WriteAllTextAsync(
                recentProjectsListFilePath,
                JsonConvert.SerializeObject(recentProjectsList, Formatting.Indented),
                Encoding.UTF8
            );
        }
        
        public async Task<List<string>> GetRecentProjectsListAsync()
        {
            var recentProjectsListFilePath = GetRecentProjectsFilePathAsync();
            
            if (!File.Exists(recentProjectsListFilePath))
                return new List<string>();

            return JsonConvert.DeserializeObject<List<string>>(await File.ReadAllTextAsync(
                recentProjectsListFilePath,
                Encoding.UTF8
            ));
        }
    }
}