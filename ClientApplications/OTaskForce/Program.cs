using ElectronNET.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace OTaskForce
{
    public class Program
    {
        public static void Main(string[] args) => new Program().ExecuteApplication(args);
        private void ExecuteApplication(string[] args) => CreateHostBuilder(args).Build().Run();

        private IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseElectron(args);
                webBuilder.UseStartup<Startup>();
            });
    }
}