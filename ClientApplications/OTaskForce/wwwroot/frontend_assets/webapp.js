﻿// JQuery and plugins
import '../../node_modules/jquery/dist/jquery.min'
import '../../node_modules/jquery-validation/dist/jquery.validate.min'
import '../../node_modules/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min'

// Bulma CSS framework
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../node_modules/bootstrap/dist/js/bootstrap.bundle.min'

// Font Awesome
import '../../node_modules/@fortawesome/fontawesome-free/css/all.min.css';

// Application-wide styles
import './application/app.js'
import './application/app.css'

// jQuery import
global.$ = require("jquery");
global.jQuery = global.$;