using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ElectronNET.API;
using ElectronNET.API.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OTaskForce.Singletons;

// ReSharper disable CA1822

namespace OTaskForce
{
    public class Startup
    {
        // ReSharper disable once NotAccessedField.Local
        private readonly IConfiguration _configuration;
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddLocalization(options => { options.ResourcesPath = "Resources"; });

            services.AddSingleton<ProjectsOperator>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseExceptionHandler("/Error");
            app.UseDeveloperExceptionPage();
            
            app.UseStaticFiles();
            app.UseRouting();
            InitializeI18N();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
            
            Task.Run(async () =>
            {
                var browserWindow = await Electron.WindowManager.CreateWindowAsync(new BrowserWindowOptions
                {
                    AutoHideMenuBar = true,
                    Center = true,
                    DarkTheme = true,
                    Title = "OTaskForce",
                    
                    MinWidth = 1100,
                    MinHeight = 650,
                    
                    Width = 1100,
                    Height = 650
                });
                return browserWindow;
            });

            void InitializeI18N()
            {
                var supportedCultures = new List<CultureInfo> { new("uk-UA") };

                var options = new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture(supportedCultures.First()),
                    SupportedCultures = supportedCultures,
                    SupportedUICultures = supportedCultures
                };

                app.UseRequestLocalization(options);
            }
        }
    }
}