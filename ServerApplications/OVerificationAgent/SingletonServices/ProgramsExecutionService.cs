﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CliWrap;
using OVerificationAgent.ProgramExecution;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;

namespace OVerificationAgent.SingletonServices
{
    public class ProgramsExecutionService
    {
        private const string TestingSystemEnvironmentVariableName = "OVERTEST_ENV";
        private const string TestingSystemEnvironmentVariableValue = "1";
        
        public async Task<ProgramExecutionResult> ExecuteProgramAsync(ProgramExecutionRequest executionRequest)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                throw new NotImplementedException();
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return await ExecuteMultiplatformAsync(executionRequest);
            }
            throw new PlatformNotSupportedException();
        }

        private async Task<ProgramExecutionResult> ExecuteMultiplatformAsync(ProgramExecutionRequest executionRequest)
        {
            const int processKilledExitCode = -1;
            const int processNormalExitCode = 0;

            try
            {
                var command = Cli.Wrap(executionRequest.FullPath)
                    .WithArguments(executionRequest.Arguments)
                    .WithWorkingDirectory(executionRequest.WorkingDirectory)
                    .WithValidation(CommandResultValidation.None)
                    .WithEnvironmentVariables(env =>
                        env.Set(TestingSystemEnvironmentVariableName, TestingSystemEnvironmentVariableValue));

                if (executionRequest.RunAsEnabled)
                {
                    command = command.WithCredentials(builder =>
                    {
                        builder.SetUserName(executionRequest.RunAsUserName);
                        
                        if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                            return;
                        
                        builder.SetPassword(executionRequest.RunAsPassword);
                        if (!string.IsNullOrWhiteSpace(executionRequest.RunAsDomain))
                            builder.SetDomain(executionRequest.RunAsDomain);
                    });
                }
                
                if (!string.IsNullOrEmpty(executionRequest.InputRedirectFileFullPath))
                {
                    command = command.WithStandardInputPipe(PipeSource.FromFile(executionRequest.InputRedirectFileFullPath));
                }
                if (!string.IsNullOrEmpty(executionRequest.OutputRedirectFileFullPath))
                {
                    command = command.WithStandardOutputPipe(PipeTarget.ToFile(executionRequest.OutputRedirectFileFullPath));
                }

                var programErrorOutputStringBuilder = new StringBuilder();
                command = command.WithStandardErrorPipe(PipeTarget.ToStringBuilder(programErrorOutputStringBuilder, Encoding.UTF8));
                
                var commandTask = command.ExecuteAsync();
                var executionResult = await ExecuteResourcesUsageWatcherAsync(commandTask.ProcessId);
                var commandResult = await commandTask;

                var programErrorOutput = programErrorOutputStringBuilder.ToString().Trim('\r', '\n', ' ');
                executionResult.ProgramErrorOutput = programErrorOutput.Length > 0 ? programErrorOutput : null;
                
                executionResult.ExitCode = commandResult.ExitCode;
                executionResult.ResourcesUsage.MaxRealTime =
                    Convert.ToInt32(Math.Floor(commandResult.RunTime.TotalMilliseconds));

                if (executionResult.ResourcesUsage.MaxRealTime > executionRequest.RuntimeLimits.MaxRealTime)
                {
                    executionResult.ExitCode = processKilledExitCode;
                    executionResult.Verdict = SingleTestVerdict.IdlingTimeout;
                }
                else if (executionResult.Verdict != SingleTestVerdict.VerificationFailed)
                {
                    /* Testing verdict was set by resources usage watcher */
                }
                else if (executionResult.ExitCode != processNormalExitCode)
                {
                    executionResult.Verdict = SingleTestVerdict.RuntimeError;
                }
                else if (executionResult.ProgramErrorOutput?.Length > 0)
                {
                    executionResult.Verdict = SingleTestVerdict.RuntimeError;
                    Console.WriteLine(executionResult.ProgramErrorOutput);
                }

                if (executionResult.Verdict == SingleTestVerdict.VerificationFailed)
                    executionResult.Verdict = SingleTestVerdict.Successful;
                return executionResult;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(null, ex);
            }

            async Task<ProgramExecutionResult> ExecuteResourcesUsageWatcherAsync(int processId)
            {
                var result = new ProgramExecutionResult();
                try
                {
                    var processInfo = Process.GetProcessById(processId);
                    while (!processInfo.HasExited)
                    {
                        processInfo.Refresh();
                        
                        result.ResourcesUsage.MaxProcessorTime = 
                            Convert.ToInt32(Math.Floor(processInfo.TotalProcessorTime.TotalMilliseconds));
                        result.ResourcesUsage.PeakWorkingSet = processInfo.PeakWorkingSet64;

                        var currentExecutionTime = Convert.ToInt32(Math.Floor(
                            (DateTime.Now - processInfo.StartTime).TotalMilliseconds));
                        if (currentExecutionTime >= executionRequest.RuntimeLimits.MaxRealTime)
                        {
                            processInfo.Kill(true);
                            result.Verdict = SingleTestVerdict.IdlingTimeout;
                            break;
                        }
                        
                        if (executionRequest.RuntimeLimits.MaxProcessorTime > 0 &&
                            result.ResourcesUsage.MaxProcessorTime >=
                            executionRequest.RuntimeLimits.MaxProcessorTime)
                        {
                            processInfo.Kill(true);
                            result.Verdict = SingleTestVerdict.ProcessorTimeLimitReached;
                            break;
                        }
                        
                        if (executionRequest.RuntimeLimits.PeakWorkingSet > 0 &&
                            result.ResourcesUsage.PeakWorkingSet >=
                            executionRequest.RuntimeLimits.PeakWorkingSet)
                        {
                            processInfo.Kill(true);
                            result.Verdict = SingleTestVerdict.WorkingSetLimitReached;
                            break;
                        }
                    }
                    await Task.CompletedTask;
                }
                catch { /* Ignore all exceptions */ }

                return result;
            }
        }
    }
}