﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Overtest.SharedLibraries.Common;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;

namespace OVerificationAgent.SingletonServices
{
    internal class TasksDataCachingService : IDisposable
    {
        private readonly SemaphoreSlim _semaphore;
        private readonly IConfiguration _configuration;
        private readonly SupportedCompilersOperator _supportedCompilersOperator;
        private readonly ILogger<TasksDataCachingService> _logger;
        
        private readonly IServiceScope _serviceScope;
        private readonly OvertestDatabaseContext _databaseContext;

        public TasksDataCachingService(IConfiguration configuration, IServiceProvider serviceProvider,
            SupportedCompilersOperator supportedCompilersOperator, ILogger<TasksDataCachingService> logger)
        {
            // Initialize a locker (semaphore with a single in/out)
            _semaphore = new SemaphoreSlim(1, 1);
            
            _configuration = configuration;
            _supportedCompilersOperator = supportedCompilersOperator;
            _logger = logger;

            _serviceScope = serviceProvider.CreateScope();
            _databaseContext = _serviceScope.ServiceProvider.GetService<OvertestDatabaseContext>()
                               ?? throw new ApplicationException($"Requested '{nameof(OvertestDatabaseContext)}', got 'null'!");
        }
        
        public async Task InitializeLocalCacheAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            _logger.LogInformation("{MethodName}: Local data storage cache initialization started!", nameof(InitializeLocalCacheAsync));
            
            // Measure time used by this method
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            
            // Get full path to the local cache directory
            var cacheDirectoryFullPath = _configuration.GetSection("storage").GetValue<string>("data_storage");
            
            // Enter a semaphore to avoid concurrency exceptions
            await _semaphore.WaitAsync(cancellationToken);
            try
            {
                _logger.LogInformation("{MethodName}: Fetching local cache entries...", nameof(InitializeLocalCacheAsync));
                
                // Fetch local cache entries list
                var localEntries = Directory.GetDirectories(cacheDirectoryFullPath)
                    .Select(Path.GetFileName)
                    .ToList();
                
                _logger.LogInformation("{MethodName}: Fetching remote entries...", nameof(InitializeLocalCacheAsync));
                
                // Fetch the list of remote entries to be locally cached
                var remoteEntries = await _databaseContext.ProgrammingTasks
                    .Where(t => _supportedCompilersOperator.SupportedCompilerIdsList.Contains(t.RequiredCompilerId))
                    .Select(s => new
                    {
                        ProgrammingTaskId = s.Id,
                        DataStorageHash = s.DataStorageHash.ToString()
                    })
                    .AsNoTracking().ToListAsync(cancellationToken);
                
                _logger.LogDebug(
                    "{MethodName}: There are {LocalEntriesCount} local cache entries and " +
                    "the total of {RemoteEntriesCount} supported programming tasks in the database",
                    nameof(InitializeLocalCacheAsync), localEntries.Count, remoteEntries.Count);
                
                /*
                 * If there are any local cache entries, check for
                 * outdated ones, ignore up to date entries and,
                 * finally, delete outdated cache we found.
                 */
                if (localEntries.Any())
                {
                    _logger.LogDebug("{MethodName}: Searching for outdated and up to date entries...",
                        nameof(InitializeLocalCacheAsync));
                    
                    var outdatedEntries = new List<string>();
                    
                    // Exclude up to date cache items and mark outdated items
                    foreach (var localEntry in localEntries)
                    {
                        // Fetch an item from list
                        var remoteItem = remoteEntries
                            .FirstOrDefault(e => e.DataStorageHash == localEntry);
                        // If item is up to date, remove it from "needs update" list
                        if (remoteItem is not null)
                        {
                            remoteEntries.Remove(remoteItem);
                            continue;
                        }
                        // Entry is outdated, we need to delete it
                        outdatedEntries.Add(localEntry);
                    }
                    
                    _logger.LogDebug(
                        "{MethodName}: There are {OutdatedEntriesCount} outdated entries and " +
                        "{UpdatedEntriesCount} entries with updates!", nameof(InitializeLocalCacheAsync),
                        outdatedEntries.Count, remoteEntries.Count);
                    
                    // Delete outdated cache entries only if needed
                    if (outdatedEntries.Any())
                    {
                        _logger.LogDebug("{MethodName}: Deleting outdated cache entries...", nameof(InitializeLocalCacheAsync));
                        
                        // Remove outdated cache entries from disk
                        foreach (var outdatedEntry in outdatedEntries)
                        {
                            // Use secure deletion method to avoid errors in the process
                            FileSystemSharedMethods.SecureDeleteDirectory(Path.Combine(cacheDirectoryFullPath, outdatedEntry));
                        }
                        
                        _logger.LogDebug("{MethodName}: Outdated cache entries deleted!", nameof(InitializeLocalCacheAsync));
                    }
                }
                
                // Create new local cache entries only if needed
                if (remoteEntries.Any())
                {
                    _logger.LogDebug("{MethodName}: Creating local cache for {RemoteEntriesCount} remote entries...",
                        nameof(InitializeLocalCacheAsync), remoteEntries.Count);
                
                    // Create local cache for new entries
                    foreach (var entry in remoteEntries)
                    {
                        try
                        {
                            await UnsafeDownloadUpdatedDataStorageAsync(entry.ProgrammingTaskId, cancellationToken);
                        }
                        catch (Exception ex)
                        {
                            // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
                            _logger.LogWarning("Caching data for '{ProgrammingTaskId}' failed: " + ex,
                                entry.ProgrammingTaskId);
                            // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
                        }
                    }

                    _logger.LogDebug("{MethodName}: Local cache creation finished!", nameof(InitializeLocalCacheAsync));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Local data storage cache corrupted and will be destroyed for safety!");
                
                try { FileSystemSharedMethods.SecureRecreateDirectory(cacheDirectoryFullPath); }
                catch (Exception innerEx)
                {
                    _logger.LogCritical(innerEx, "Cannot safely destroy the local cache storage! It is a critical exception, terminating...");
                    throw new ApplicationException("Local storage is corrupted!", innerEx);
                }
            }
            finally { _semaphore.Release(); }
            
            // Stop the stopwatch and log the measurement
            stopWatch.Stop();
            _logger.LogInformation(
                "{MethodName}: Local data storage cache initialization finished! " +
                "The process took {TimeElapsed} ms to finish ;)",
                nameof(InitializeLocalCacheAsync), stopWatch.ElapsedMilliseconds);
        }
        
        public async Task<(string fullPath, bool updated)> GetTaskDataFullPathAsync(Guid programmingTaskId,
            CancellationToken cancellationToken = default)
        {
            var cacheUpdateResult = await TryUpdateCacheEntryAsync(programmingTaskId, cancellationToken);
            
            if (!cacheUpdateResult.taskExists)
                throw new ApplicationException($"Programming task with id '{programmingTaskId}' does not exist!");

            var verificationDataFullPath = Path.Combine(GetDataStoragePath(cacheUpdateResult.normalizedHash));
            return (verificationDataFullPath, cacheUpdateResult.updated);
        }
        
        public async Task<(bool taskExists, bool updated, string normalizedHash)> TryUpdateCacheEntryAsync(
            Guid programmingTaskId, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            // Use semaphore here to prevent concurrency issues from happening
            await _semaphore.WaitAsync(cancellationToken);
            try
            {
                // Verify that the specified programming task still exists
                if (!await _databaseContext.ProgrammingTasks.Where(t => t.Id == programmingTaskId).AnyAsync(cancellationToken))
                    return (false, false, null);
                
                // Try to update the cache. Warning: calling this method without semaphore is unsafe!
                var (updated, normalizedHash) = await UnsafeDownloadUpdatedDataStorageAsync(programmingTaskId, cancellationToken);
                
                // Cache for the current task updated successfully
                return (true, updated, normalizedHash);
            }
            // Finally, release the semaphore
            finally { _semaphore.Release(); }
        }
        
        private async ValueTask<(bool updated, string normalizedHash)> UnsafeDownloadUpdatedDataStorageAsync(
            Guid programmingTaskId, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            _logger.LogDebug("{MethodName}: Trying to update cache for programming task '{ProgrammingTaskId}'...",
                nameof(UnsafeDownloadUpdatedDataStorageAsync), programmingTaskId);
            
            // Fetch data storage hash from the database and normalize it
            var dataStoreHash = await _databaseContext.ProgrammingTasks.AsNoTracking()
                .Where(t => t.Id == programmingTaskId)
                .Select(s => s.DataStorageHash.ToString())
                .FirstAsync(cancellationToken);
            
            // If verification data for this task is up to deate, return false
            if (DataStorageExists(dataStoreHash))
            {
                _logger.LogDebug("{MethodName}: Cache for programming task '{ProgrammingTaskId}' is up to date!",
                    nameof(UnsafeDownloadUpdatedDataStorageAsync), programmingTaskId);
                return (false, dataStoreHash);
            }
            
            // Fetch ZIP archive containing task data from the database in form of bytes array
            var dataStorage = await _databaseContext.ProgrammingTasks.AsNoTracking()
                .Where(t => t.Id == programmingTaskId)
                .Select(s => s.DataStorage)
                .FirstAsync(cancellationToken);
            
            // Prepare temp and cache paths for usage
            var localStorageConfiguration = _configuration.GetSection("storage");
            var taskTempPath = Path.Combine(localStorageConfiguration.GetValue<string>("temp_data"), dataStoreHash + ".zip");
            var taskCachePath = GetDataStoragePath(dataStoreHash);
            
            // Prevent fatal errors by using try block and deleting broken temp files and cache data
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                
                // Write data store raw value (byte array) to file
                await File.WriteAllBytesAsync(taskTempPath, dataStorage, cancellationToken);
                // Create verification data cache directory for the current task
                Directory.CreateDirectory(taskCachePath);
                
                // Unpack newly-created ZIP archive to the cache directory
                ZipFile.ExtractToDirectory(
                    sourceArchiveFileName: taskTempPath,
                    destinationDirectoryName: taskCachePath,
                    Encoding.UTF8, true);
                
                /*
                 * Data storage of the current programming task is ready now,
                 * so we should prepare it for long-term usage. For example,
                 * we can upper the performance of verification process by
                 * executing pre-compilation of verification (TestLib) scripts
                 * and author solution for the programming task at this step.
                 */
                await UnsafePrepareDataStorageAsync(programmingTaskId, taskCachePath);
            }
            catch (Exception ex)
            {
                // Try to delete cache directory
                try { Directory.Delete(taskCachePath, true); }
                catch (Exception) { /* Ignore all exceptiona */ }
                
                throw new ApplicationException($"Archive extraction failed for programming task '{programmingTaskId}'!", ex);
            }
            finally
            {
                // Try to delete temp file
                try { File.Delete(taskTempPath); }
                catch (Exception) { /* Ignore all exceptiona */ }
            }

            _logger.LogDebug("{MethodName}: Cache for programming task '{ProgrammingTaskId}' updated successfully!",
                nameof(UnsafeDownloadUpdatedDataStorageAsync), programmingTaskId);
            
            // Return true to inform that the verification data was updated
            return (true, dataStoreHash);
        }

        // ReSharper disable once UnusedParameter.Local
        private async ValueTask UnsafePrepareDataStorageAsync(Guid progrmmingTaskId, string dataStoragePath)
        {
            var vdc = VerificationDataConfiguration.GetConfigurationFromJson(dataStoragePath);
            var vdDirectoryFullPath = VerificationDataConfiguration.GetVerificationDataDirectoryPath(dataStoragePath);
            
            /*
             * Pre-compile author's solution for
             * the current programming task.
             */
            
            var authorSolutionDirectoryPath = Path.Combine(vdDirectoryFullPath, vdc.AuthorSolution.SourceCodePath);
            var compiler = _supportedCompilersOperator.GetCompilerById(new Guid(vdc.AuthorSolution.CompilerId));
            var compilationResult = compiler.ExecuteCompiler(authorSolutionDirectoryPath);
            
            if (!compilationResult.IsSuccessful)
            {
                _logger.LogWarning(
                    "{MethodName}: {ProgrammingTaskId}: Author solution compilation failed!",
                    nameof(UnsafePrepareDataStorageAsync), progrmmingTaskId);
                throw new ApplicationException(compilationResult.CompilationOutput);
            }
            
            _logger.LogDebug(
                "{MethodName}: {ProgrammingTaskId}: Author solution compilation finished successfully!",
                nameof(UnsafePrepareDataStorageAsync), progrmmingTaskId);
            
            await Task.CompletedTask;
        }
        
        private bool DataStorageExists(string normalizedHash) => Directory.Exists(GetDataStoragePath(normalizedHash));

        private string GetDataStoragePath(string normalizedHash)
        {
            var localStorageConfiguration = _configuration.GetSection("storage");
            var cachePath = localStorageConfiguration.GetValue<string>("data_storage");
            return Path.Combine(cachePath, normalizedHash);
        }
        
        public void Dispose()
        {
            _semaphore?.Dispose();
            _serviceScope?.Dispose();
        }
    }
}