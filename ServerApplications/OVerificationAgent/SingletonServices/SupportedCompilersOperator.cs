﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers;

namespace OVerificationAgent.SingletonServices
{
    internal class SupportedCompilersOperator : IDisposable
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<SupportedCompilersOperator> _logger;

        private readonly List<SupportedCompilerEntity> _compilerEntities;
        public List<Guid> SupportedCompilerIdsList { get; }

        public SupportedCompilersOperator(IConfiguration configuration, ILogger<SupportedCompilersOperator> logger)
        {
            _configuration = configuration;
            _logger = logger;
            
            // TODO: Create a new scope and fetch database context service
            // to verify that all supported compiler exist in the database
            
            _compilerEntities = new List<SupportedCompilerEntity>();
            SupportedCompilerIdsList = new List<Guid>();
        }

        public void Initialize()
        {
            _compilerEntities.Clear();
            SupportedCompilerIdsList.Clear();

            var compilersConfigurationSection = _configuration.GetSection("compilers");
            var supportedCompilersConfiguration = compilersConfigurationSection.GetChildren();
            
            foreach (var compilerConfiguration in supportedCompilersConfiguration)
            {
                if (!Guid.TryParse(compilerConfiguration.Key, out var compilerId))
                {
                    _logger.LogCritical("Compiler ID '{CompilerId}' is invalid and cannot be parsed into Guid!", compilerConfiguration.Key);
                    throw new ApplicationException();
                }
                
                if (!File.Exists(compilerConfiguration.Value))
                {
                    _logger.LogCritical("Compiler with ID '{CompilerId}': script file not found!", compilerConfiguration.Key);
                }
                
                _compilerEntities.Add(new SupportedCompilerEntity(compilerId, compilerConfiguration.Value));
                SupportedCompilerIdsList.Add(compilerId);
                
                _logger.LogDebug("Successfully added compiler with ID '{CompilerId}'!", compilerId);
            }
        }
        
        public SupportedCompilerEntity GetCompilerById(Guid compilerId)
        {
            // TODO: Check if concurrent usage fails
            var compiler = _compilerEntities.FirstOrDefault(e => e.CompilerId == compilerId);
            
            if (compiler is null)
                throw new ApplicationException("Compiler with specified ID was not found!");

            return compiler;
        }

        public void Dispose()
        {
            try
            {
                SupportedCompilerIdsList?.Clear();

                if (_compilerEntities is null)
                    return;

                for (var i = 0; i < _compilerEntities.Count; i++)
                {
                    _compilerEntities[i].Dispose();
                    _compilerEntities[i] = null;
                }
            }
            catch (Exception) { /* Ignore all exceptions */ }
        }
    }
}