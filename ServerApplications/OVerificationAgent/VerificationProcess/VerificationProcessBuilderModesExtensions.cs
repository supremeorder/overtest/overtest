﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OVerificationAgent.Helpers;
using OVerificationAgent.ProgramExecution;
using OVerificationAgent.SingletonServices;
using Overtest.SharedLibraries.Common;
using Overtest.SharedLibraries.ProgrammingTasksShared.Operators;
using Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace OVerificationAgent.VerificationProcess
{
    internal static class VerificationProcessBuilderModesExtensions
    {
        public static VerificationProcessBuilder AddReleaseVerificationMode(this VerificationProcessBuilder vpb)
        {
            return vpb.AddTaskToQueue(async processBuilder =>
            {
                // Continue execution of this method only on 'ReleaseMode' verification type
                if (processBuilder.TaskSolution.VerificationType != TaskSolution.SolutionVerificationType.ReleaseMode)
                    return;
                // Proceed only on successful task solution program compilation
                if (!processBuilder.TaskSolutionVerificationResult.CompilationResult.IsSuccessful)
                    return;
                
                // Get an instance of application configuration storage service
                var appConfiguration = processBuilder.ServiceProvider.GetService<IConfiguration>()
                                       ?? throw new ApplicationException();
                var supportedCompilersOperator = processBuilder.ServiceProvider.GetService<SupportedCompilersOperator>()
                                                 ?? throw new ApplicationException();
                var programsExecutionService = processBuilder.ServiceProvider.GetService<ProgramsExecutionService>()
                                               ?? throw new ApplicationException();
                // Store release mode configuration in the local variable
                var releaseModeConfig = processBuilder.VerificationProcessConfiguration.ReleaseConfiguration;
                
                var userProgramCompiler = supportedCompilersOperator.GetCompilerById(
                    processBuilder.TaskSolution.CompilerId);
                var authorProgramCompiler = supportedCompilersOperator.GetCompilerById(new Guid(
                    processBuilder.VerificationProcessConfiguration.AuthorSolution.CompilerId));
                
                // Create a new instance of the TestLib operator
                using var testLibOperator = new TestLibOperator(
                    processBuilder.VerificationProcessConfiguration.TestLib,
                    processBuilder.VerificationData.FullPath);

                processBuilder.TaskSolutionVerificationResult.TestResults ??= new List<SingleTestResult>();
                
                // Create and initialize variables needed to
                // implement idling timeouts limiting feature.
                var idlingTimeoutReachedTestsCount = 0;
                var idlingTimeoutsLimitReached = false;
                
                // Catch all exceptions in a loop
                try
                {
                    for (var currentTestId = 0; currentTestId < releaseModeConfig.Tests.Count; currentTestId++)
                    {
                        // Get verification process configuration for the current test
                        var currentTestInfo = processBuilder.VerificationProcessConfiguration.ReleaseConfiguration.Tests[currentTestId];
                        currentTestInfo.RuntimeLimits ??= processBuilder.VerificationProcessConfiguration.RuntimeLimits;
                        
                        if (idlingTimeoutsLimitReached)
                        {
                            processBuilder.TaskSolutionVerificationResult.TestResults.Add(new SingleTestResult
                            {
                                TestTitle = currentTestInfo.Title, Verdict = SingleTestVerdict.VerificationFailed,
                                ExitCode = -1, ProcessorTimeUsed = 0, RealExecutionTimeUsed = 0, WorkingSetUsed = 0,
                                RuntimeLimits = currentTestInfo.RuntimeLimits
                            });
                            return;
                        }
                        
                        // Execute single test and add it's results to the "TestResults" section of TaskSolutionVerificationResult
                        var currentTestCompletionResult = await ExecuteSingleTestAsync(processBuilder,
                            appConfiguration, testLibOperator, programsExecutionService, currentTestInfo,
                            currentTestId, userProgramCompiler, authorProgramCompiler);
                        processBuilder.TaskSolutionVerificationResult.TestResults.Add(currentTestCompletionResult);
                        
                        // Count idling timeouts on all tests
                        if (currentTestCompletionResult.Verdict == SingleTestVerdict.IdlingTimeout)
                            idlingTimeoutReachedTestsCount++;
                        
                        // If idling timeouts limit reached, break the loop
                        if (releaseModeConfig.RealTimeKillsLimit > 0 &&
                            idlingTimeoutReachedTestsCount > releaseModeConfig.RealTimeKillsLimit)
                        {
                            idlingTimeoutsLimitReached = true;
                        }
                    }
                }
                catch (Exception)
                {
                    // For safety, try to clear the list of single test completion results
                    processBuilder.TaskSolutionVerificationResult.TestResults?.Clear();
                    // Finally, re-throw an exception
                    throw;
                }
            });
        }

        private static async Task<SingleTestResult> ExecuteSingleTestAsync(VerificationProcessBuilder processBuilder,
            IConfiguration appConfiguration, TestLibOperator testLibOperator, ProgramsExecutionService programsExecutionService,
            SingleTestInfo testInfo, int testId, SupportedCompilerEntity userProgramCompiler, SupportedCompilerEntity authorProgramCompiler)
        {
            // Prepare single test result
            var testResult = new SingleTestResult(testInfo.Title, testInfo.RuntimeLimits);
            
            // Get the full path to the application's temp directory
            var tempStoragePath = appConfiguration.GetSection("storage").GetValue<string>("temp_data");
            // Create and use short-living directories for the current test
            using var inputDataDirectory = DisposableDirectoryContainer.CreateTempDirectory(tempStoragePath);
            using var userProgramDirectory = DisposableDirectoryContainer.CreateTempDirectory(
                tempStoragePath, processBuilder.TaskSolutionDirectory.FullPath);
            
            // Execute input generator for the current test
            if (!testLibOperator.ExecuteGenerator(testId, inputDataDirectory.FullPath))
                throw new ApplicationException($"Input data generation failed for the test #{testId}!");
            
            // Copy input data to temporary user solution's directory
            FileSystemSharedMethods.SecureCopyDirectory(
                inputDataDirectory.FullPath,
                userProgramDirectory.FullPath
            );

            var runAsConfiguration = appConfiguration.GetSection("runner").GetSection("runas");
            var programExecutionRequest = new ProgramExecutionRequest
            {
                RunAsEnabled = runAsConfiguration.GetValue<bool>("enabled"),
                RunAsUserName = runAsConfiguration.GetValue<string>("username"),
                RunAsPassword = runAsConfiguration.GetValue<string>("password"),
                RunAsDomain = runAsConfiguration.GetValue<string>("domain"),
                
                InputRedirectFileName = testLibOperator.Configuration.DefaultInputFileName,
                OutputRedirectFileName = testLibOperator.Configuration.DefaultOutputFileName,
                
                RuntimeLimits = testInfo.RuntimeLimits
            };

            var userProgramExecutionParameters = userProgramCompiler.GetExecutionParameters(
                userProgramDirectory.FullPath, string.Empty);
            
            programExecutionRequest.FullPath = userProgramExecutionParameters.ExecutableFullName;
            programExecutionRequest.Arguments = userProgramExecutionParameters.Arguments;
            programExecutionRequest.WorkingDirectory = userProgramDirectory.FullPath;

            bool isUserProgramExecutionSucceeded;
            try
            {
                var result = await programsExecutionService.ExecuteProgramAsync(programExecutionRequest);
                
                isUserProgramExecutionSucceeded = result.Verdict == SingleTestVerdict.Successful;
                if (!isUserProgramExecutionSucceeded)
                    testResult.Verdict = result.Verdict;
                
                testResult.ExitCode = result.ExitCode;
                
                testResult.ProcessorTimeUsed = result.ResourcesUsage.MaxProcessorTime;
                testResult.WorkingSetUsed = result.ResourcesUsage.PeakWorkingSet;
                testResult.RealExecutionTimeUsed = result.ResourcesUsage.MaxRealTime;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"User program execution failed on test #{testId}!", ex);
            }
            
            if (testInfo.ExecuteAuthorSolution)
            {
                // Create and use short-living directory for the author's program on the current test
                using var authorProgramDirectory = DisposableDirectoryContainer.CreateTempDirectory(
                    tempStoragePath, processBuilder.AuthorSolutionDirectoryFullPath);
                
                // Copy input data to temporary author solution's directory
                FileSystemSharedMethods.SecureCopyDirectory(inputDataDirectory.FullPath,
                    authorProgramDirectory.FullPath);
                
                var authorProgramExecutionParameters = authorProgramCompiler.GetExecutionParameters(
                    authorProgramDirectory.FullPath, string.Empty);
                programExecutionRequest.FullPath = authorProgramExecutionParameters.ExecutableFullName;
                programExecutionRequest.Arguments = authorProgramExecutionParameters.Arguments;
                programExecutionRequest.WorkingDirectory = authorProgramDirectory.FullPath;

                try
                {
                    var result = await programsExecutionService.ExecuteProgramAsync(programExecutionRequest);
                    if (result.Verdict != SingleTestVerdict.Successful)
                        throw new InvalidOperationException($"Author program exited with {result.Verdict.ToString()} verdict!");
                }
                catch (Exception ex)
                {
                    throw new ApplicationException($"Author program execution failed on test #{testId}!", ex);
                }

                if (isUserProgramExecutionSucceeded)
                {
                    // Execute an output checker with author program output path
                    testResult.Verdict = testLibOperator.ExecuteChecker(testId,
                        userProgramDirectory.FullPath, authorProgramDirectory.FullPath);
                }
            }
            else if (isUserProgramExecutionSucceeded)
            {
                // Execute an output checker with author program output path
                testResult.Verdict = testLibOperator.ExecuteChecker(testId, userProgramDirectory.FullPath);
            }
            
            // Return a single test completion result
            return testResult;
        }
    }
}