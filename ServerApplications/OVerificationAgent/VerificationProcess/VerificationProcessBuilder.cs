﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using OVerificationAgent.Helpers;
using OVerificationAgent.SingletonServices;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace OVerificationAgent.VerificationProcess
{
    internal class VerificationProcessBuilder : IAsyncDisposable
    {
        private List<Func<VerificationProcessBuilder, Task>> _verificationProcessTasksQueue;
        private readonly CancellationTokenSource _vptCancellationTokenSource;
        private Exception _verificationProcessException;
        
        public IServiceProvider ServiceProvider { get; }
        public Guid TaskSolutionId { get; }
        public ILogger Logger { get; }
        
        public TaskSolution TaskSolution { get; private set; }
        public DisposableDirectoryContainer TaskSolutionDirectory { get; private set; }
        public TaskSolutionVerificationResult TaskSolutionVerificationResult { get; private set; }
        
        public DisposableDirectoryContainer VerificationData { get; set; }
        public VerificationDataConfiguration VerificationProcessConfiguration { get; set; }
        public string AuthorSolutionDirectoryFullPath { get; set; }

        private VerificationProcessBuilder(IServiceProvider serviceProvider, Guid taskSolutionId)
        {
            _verificationProcessTasksQueue = new List<Func<VerificationProcessBuilder, Task>>();
            // Create a new cancellation token source so it can be used to handle errors
            _vptCancellationTokenSource = new CancellationTokenSource();
            // 
            _verificationProcessException = null;
            
            // Store a reference to the service provider
            ServiceProvider = serviceProvider;
            // Store a unique identifier of task solution
            TaskSolutionId = taskSolutionId;
            // Initialize current class logger
            Logger = LogManager.GetCurrentClassLogger();
            
            Logger.Debug("{ObjectType} for task solution '{TaskSolutionId}' created, waiting for commands...",
                nameof(VerificationProcessBuilder), TaskSolutionId);
        }

        public static VerificationProcessBuilder CreateBuilder(IServiceProvider serviceProvider, Guid taskSolutionId)
        {
            return new VerificationProcessBuilder(serviceProvider, taskSolutionId);
        }
        
        // ReSharper disable once UnusedMethodReturnValue.Global
        public VerificationProcessBuilder AddTaskToQueue(Func<VerificationProcessBuilder, Task> nextTask)
        {
            _verificationProcessTasksQueue.Add(nextTask);
            // Return a reference to self
            return this;
        }

        private async Task ExecuteVerificationProcessTaskAsync()
        {
            foreach (var queuedTask in _verificationProcessTasksQueue)
            {
                // Execute the newly-added task
                var task = queuedTask(this);

                try { await task; }
                catch (Exception) { /* Ignore all exceptions */ }

                if (task.IsCanceled || task.IsFaulted || task.Exception is not null)
                {
                    _verificationProcessException = task.Exception;
                    _vptCancellationTokenSource.Cancel();
                    break;
                }
            }
        }

        private async Task FetchTaskSolutionFromDatabaseAsync(CancellationToken cancellationToken = default)
        {
            // Get application configuration service
            var appConfiguration = ServiceProvider.GetService<IConfiguration>()
                                   ?? throw new ApplicationException();
            var appTempDirectoryPath = appConfiguration
                .GetSection("storage")
                .GetValue<string>("temp_data");

            // Get database context service
            var databaseContext = ServiceProvider.GetService<OvertestDatabaseContext>()
                                  ?? throw new ApplicationException();
            // Get supported compilers operator service
            var supportedCompilersOperator = ServiceProvider.GetService<SupportedCompilersOperator>()
                                             ?? throw new ApplicationException();

            // Load task solution details from the database
            TaskSolution = await databaseContext.ProgrammingTaskSolutions
                .Include(i => i.ProgrammingTask)
                .Where(s => s.Id == TaskSolutionId)
                .Select(s => new TaskSolution
                {
                    Id = s.Id,
                    Created = s.Created,
                    VerificationType = s.VerificationType,
                    VerificationStatus = s.VerificationStatus,
                    SourceCode = s.SourceCode,
                    AuthorId = s.AuthorId,
                    ProgrammingTaskId = s.ProgrammingTaskId,
                    ProgrammingTask = new ProgrammingTask
                    {
                        Difficulty = s.ProgrammingTask.Difficulty
                    },
                    CompilerId = s.CompilerId
                }).AsNoTracking().FirstAsync(cancellationToken);

            // Initialize the current task solution directory
            TaskSolutionDirectory = DisposableDirectoryContainer.CreateTempDirectory(appTempDirectoryPath);
            
            // Write source code of the current solution to the
            // corresponding file in the task solution directory.
            var selectedCompiler = supportedCompilersOperator.GetCompilerById(TaskSolution.CompilerId);
            await File.WriteAllTextAsync(
                selectedCompiler.GetSourceCodeFileFullPath(TaskSolutionDirectory.FullPath),
                TaskSolution.SourceCode, Encoding.UTF8, cancellationToken);
        }

        public async Task SetVerificationStatusAsync(TaskSolution.SolutionVerificationStatus updatedStatus,
            CancellationToken cancellationToken = default)
        {
            // Throw an error if operation is cancelled
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                // Try to get an instance of database context from services store
                var databaseContext = ServiceProvider.GetService<OvertestDatabaseContext>();
                if (databaseContext is null)
                    throw new ApplicationException("Service provider returned null, not database context instance!");

                // Fetch current state of the verification request from the database
                var dbObject = await databaseContext.ProgrammingTaskSolutions
                    .Where(s => s.Id == TaskSolutionId)
                    .Select(s => new TaskSolution { Id = s.Id, VerificationStatus = s.VerificationStatus })
                    .AsNoTracking().FirstAsync(cancellationToken);

                // Store previous verification status
                var previousVerificationStatus = dbObject.VerificationStatus;
                // Update verification status (localy and remote)
                dbObject.VerificationStatus = TaskSolution.VerificationStatus = updatedStatus;

                // Attact the temporary db object to the watcher
                databaseContext.ProgrammingTaskSolutions.Attach(dbObject);

                // [VerificationStatus] is an concurrency stamp, so passing original value is required
                databaseContext.Entry(dbObject)
                    .Property(p => p.VerificationStatus)
                    .OriginalValue = previousVerificationStatus;

                // Inform EF Core that [VerificationStatus] property is modified
                databaseContext.Entry(dbObject)
                    .Property(p => p.VerificationStatus)
                    .IsModified = true;

                // Apply changes made to the database
                await databaseContext.SaveChangesAsync(cancellationToken);
                // Don't forget to detach temporary object from the watcher
                databaseContext.Entry(dbObject).State = EntityState.Detached;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("TaskSolution verification status update failed with exception!", ex);
            }
        }
        
        private async Task UploadVerificationResultsAsync(CancellationToken cancellationToken = default)
        {
            var dbObject = new TaskSolution
            {
                Id = TaskSolutionId,
                ProcessingTime = TaskSolution.ProcessingTime,
                VerificationStatus = TaskSolution.VerificationStatus,
                RawVerificationResult = TaskSolution.RawVerificationResult,
                Adjudgement = TaskSolution.Adjudgement,
                GivenRating = TaskSolution.GivenRating
            };
            
            var databaseContext = ServiceProvider.GetService<OvertestDatabaseContext>()
                                  ?? throw new ApplicationException();

            databaseContext.ProgrammingTaskSolutions.Attach(dbObject);
            
            databaseContext.Entry(dbObject)
                .Property(p => p.VerificationStatus)
                .OriginalValue = dbObject.VerificationStatus;
            databaseContext.Entry(dbObject)
                .Property(p => p.VerificationStatus)
                .IsModified = false;
            // Task solution processing time
            databaseContext.Entry(dbObject)
                .Property(p => p.ProcessingTime)
                .IsModified = true;
            // Raw verification result
            databaseContext.Entry(dbObject)
                .Property(p => p.RawVerificationResult)
                .IsModified = true;
            // Solution adjudgement
            databaseContext.Entry(dbObject)
                .Property(p => p.Adjudgement)
                .IsModified = true;
            // Given rating
            databaseContext.Entry(dbObject)
                .Property(p => p.GivenRating)
                .IsModified = true;
            
            await databaseContext.SaveChangesAsync(cancellationToken);
            databaseContext.Entry(dbObject).State = EntityState.Detached;
            
            await SetVerificationStatusAsync(TaskSolution.SolutionVerificationStatus.Verified, cancellationToken);
        }
        
        public async Task ExecuteAsync(CancellationToken cancellationToken = default)
        {
            Logger.Debug("{MethodName}: Starting verification process of '{TaskSolutionId}'...", nameof(ExecuteAsync), TaskSolutionId);
            Logger.Debug("{TaskSolutionId}': Initializing...", TaskSolutionId);
            cancellationToken.ThrowIfCancellationRequested();
            
            // Save current date and time as the start point of the verification process
            var processingStarted = DateTime.UtcNow;
            
            /*
             * Initialize requred variables and properties
             */
            
            // Fetch information about the current task solution from the database
            await FetchTaskSolutionFromDatabaseAsync(cancellationToken);
            
            // Initialize an empty task solution verification result
            TaskSolutionVerificationResult = new TaskSolutionVerificationResult
            {
                CompilationResult = null,
                TestResults = new List<SingleTestResult>()
            };
            
            /*
             * Start an execution of the verification process
             */
            
            Logger.Debug("{TaskSolutionId}': Starting execution of pre-built verification process task...", TaskSolutionId);
            try { await ExecuteVerificationProcessTaskAsync(); }
            catch (TaskCanceledException) { /* This exception is a part of error handling logics */ }
            Logger.Debug("{TaskSolutionId}': Execution of pre-built verification process task finished!", TaskSolutionId);

            // Save current date and time as the end point of verification process
            var processingFinished = DateTime.UtcNow;
            // Get the task solution processing time span
            TaskSolution.ProcessingTime = processingFinished - processingStarted;
            
            /*
             * Check whether verification process task faulted (we use
             * cancellation token source to handle this scenario).
             */

            if (_vptCancellationTokenSource.IsCancellationRequested)
            {
                // If exception type is 'AggregateException', we need to unpack it safely
                if (_verificationProcessException is not null &&
                    _verificationProcessException.GetType() == typeof(AggregateException) &&
                    _verificationProcessException.InnerException is not null)
                {
                    _verificationProcessException = _verificationProcessException.InnerException;
                }
                
                // Save exception as the verification result error message
                TaskSolutionVerificationResult.ErrorMessage = _verificationProcessException?.ToString();
                
                // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
                Logger.Warn("Verification process for '{TaskSolutionId}' failed with exception: " +
                            _verificationProcessException, TaskSolutionId);
            }
            
            /*
             * Format and upload results of the verification process
             */
            
            // Set raw verification result (store serialized data in JSON format as a byte array)
            TaskSolution.RawVerificationResult = Encoding.UTF8.GetBytes(TaskSolutionVerificationResult.ToJsonString());
            
            // Upload verification results to the database
            await UploadVerificationResultsAsync(cancellationToken);
        }
        
        async ValueTask IAsyncDisposable.DisposeAsync()
        {
            // Dispose the cancellation token source used to
            // handle faults in the verification process.
            _vptCancellationTokenSource?.Cancel();
            _vptCancellationTokenSource?.Dispose();
            
            TaskSolutionDirectory?.Dispose();
            VerificationData?.Dispose();
            // As for now, there's nothing to do here
            await Task.CompletedTask;
        }
    }
}