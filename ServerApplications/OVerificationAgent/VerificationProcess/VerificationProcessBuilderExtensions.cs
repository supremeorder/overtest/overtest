﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OVerificationAgent.Helpers;
using OVerificationAgent.SingletonServices;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace OVerificationAgent.VerificationProcess
{
    internal static class VerificationProcessBuilderExtensions
    {
        public static VerificationProcessBuilder PrepareVerificationProcessConfiguration(this VerificationProcessBuilder vpb)
        {
            return vpb.AddTaskToQueue(async processBuilder =>
            {
                const string verificationDataDirectoryName = "verificationdata";

                // TODO: Set verification process status to 'Preparation'

                // Get an instance of the task data caching service
                var taskDataCachingService = processBuilder.ServiceProvider.GetService<TasksDataCachingService>()
                                             ?? throw new ApplicationException();

                // Get an instance of application configuration service
                var applicationConfiguration = processBuilder.ServiceProvider.GetService<IConfiguration>()
                                               ?? throw new ApplicationException();

                var applicationTempDirectoryPath = applicationConfiguration
                    .GetSection("storage")
                    .GetValue<string>("temp_data");

                var (taskDataFullPath, _) = await taskDataCachingService
                    .GetTaskDataFullPathAsync(processBuilder.TaskSolution.ProgrammingTaskId);

                var originalVerificationDataPath = Path.Combine(
                    taskDataFullPath, verificationDataDirectoryName);
                
                processBuilder.VerificationData = DisposableDirectoryContainer.CreateTempDirectory(
                    applicationTempDirectoryPath, originalVerificationDataPath);
                processBuilder.VerificationProcessConfiguration = VerificationDataConfiguration
                    .GetConfigurationFromJson(taskDataFullPath);
                
                // Get the full path to the author's solution for the current programming task
                processBuilder.AuthorSolutionDirectoryFullPath = Path.Combine(processBuilder.VerificationData.FullPath,
                    processBuilder.VerificationProcessConfiguration.AuthorSolution.SourceCodePath);
            });
        }

        public static VerificationProcessBuilder CompileTaskSolution(this VerificationProcessBuilder vpb)
        {
            return vpb.AddTaskToQueue(async processBuilder =>
            {
                // Get an instance of supported compilers operator
                var supportedCompilersOperator = processBuilder.ServiceProvider.GetService<SupportedCompilersOperator>()
                                                 ?? throw new ApplicationException();

                // Fetch an instance of supported compiler entry used to write the current task solution
                var selectedCompiler =
                    supportedCompilersOperator.GetCompilerById(processBuilder.TaskSolution.CompilerId);

                // Execute compilation step
                processBuilder.TaskSolutionVerificationResult.CompilationResult =
                    selectedCompiler.ExecuteCompiler(processBuilder.TaskSolutionDirectory.FullPath);

                await Task.CompletedTask;
            });
        }

        public static VerificationProcessBuilder PrepareVerificationResults(this VerificationProcessBuilder vpb)
        {
            return vpb.AddTaskToQueue(async processBuilder =>
            {
                // TODO: If in competition mode, check if 'ManualJudging' verification status requested
                
                await processBuilder.SetVerificationStatusAsync(
                    TaskSolution.SolutionVerificationStatus.AutomaticJudging);

                switch (processBuilder.TaskSolution.VerificationType)
                {
                    // SYNTAX verification type
                    case TaskSolution.SolutionVerificationType.SyntaxMode:
                        processBuilder.TaskSolution.Adjudgement = TaskSolution.SolutionAdjudgementType.ZeroSolution;
                        processBuilder.TaskSolution.GivenRating = 0;
                        return;
                    
                    // DEBUG verification type
                    case TaskSolution.SolutionVerificationType.DebugMode:
                        throw new NotImplementedException("Debug verification type is not yet implemented!");
                    
                    // RELEASE verification type (when compilation was successful)
                    case TaskSolution.SolutionVerificationType.ReleaseMode
                        when processBuilder.TaskSolutionVerificationResult.CompilationResult.IsSuccessful:
                    {
                        var totalTestsCount = processBuilder.TaskSolutionVerificationResult.TestResults.Count;
                        var passedTestsCount = processBuilder.TaskSolutionVerificationResult.TestResults
                            .Count(r => r.Verdict == SingleTestVerdict.Successful);
                        var programmingTaskDifficulty = processBuilder.TaskSolution.ProgrammingTask.Difficulty;
                        
                        // Is [CompleteSolution] adjudgement
                        if (totalTestsCount <= 0 || passedTestsCount == totalTestsCount)
                        {
                            processBuilder.TaskSolution.Adjudgement = TaskSolution.SolutionAdjudgementType.CompleteSolution;
                            processBuilder.TaskSolution.GivenRating = programmingTaskDifficulty;
                        }
                        // Is [ZeroSolution] adjudgement
                        else if (passedTestsCount == 0)
                        {
                            processBuilder.TaskSolution.Adjudgement = TaskSolution.SolutionAdjudgementType.ZeroSolution;
                            processBuilder.TaskSolution.GivenRating = 0;
                        }
                        // Is [PartialSolution] adjudgement
                        else
                        {
                            var givenRating = (passedTestsCount / totalTestsCount) * programmingTaskDifficulty;
                            processBuilder.TaskSolution.Adjudgement = TaskSolution.SolutionAdjudgementType.PartialSolution;
                            processBuilder.TaskSolution.GivenRating = givenRating;
                        }
                        
                        break;
                    }
                    
                    // RELEASE verification type (when compilation was not successful)
                    case TaskSolution.SolutionVerificationType.ReleaseMode:
                        processBuilder.TaskSolution.Adjudgement = TaskSolution.SolutionAdjudgementType.ZeroSolution;
                        processBuilder.TaskSolution.GivenRating = 0;
                        break;
                }
            });
        }
    }
}