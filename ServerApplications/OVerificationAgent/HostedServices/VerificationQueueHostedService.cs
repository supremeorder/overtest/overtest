﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OVerificationAgent.SingletonServices;
using OVerificationAgent.VerificationProcess;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace OVerificationAgent.HostedServices
{
    internal class VerificationQueueHostedService : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<VerificationQueueHostedService> _logger;
        private readonly SupportedCompilersOperator _supportedCompilersOperator;
        private readonly IServiceProvider _serviceProvider;
        private readonly TasksDataCachingService _tasksDataCachingService;

        public VerificationQueueHostedService(IConfiguration configuration, IServiceProvider serviceProvider,
            ILogger<VerificationQueueHostedService> logger, SupportedCompilersOperator supportedCompilersOperator,
            TasksDataCachingService tasksDataCachingService)
        {
            _configuration = configuration;
            _logger = logger;
            _supportedCompilersOperator = supportedCompilersOperator;
            _serviceProvider = serviceProvider;
            _tasksDataCachingService = tasksDataCachingService;
        }
        
        private async Task InitializeServicesAsync(CancellationToken cancellationToken = default)
        {
            // Respect given cancellation token
            cancellationToken.ThrowIfCancellationRequested();
            // Initialize supported compilers service
            _supportedCompilersOperator.Initialize();
            // Initialize (or refresh) local programming tasks data cache
            await _tasksDataCachingService.InitializeLocalCacheAsync(cancellationToken);
        }
        
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            // Initialize essential services (for example, local storage, etc)
            await InitializeServicesAsync(cancellationToken);
            
            // Get number of allowed concurrent verification tasks
            var allowedConcurrency = new Func<int>(() =>
            {
                var desiredConcurrency = _configuration.GetValue<int>("concurrency");
                return desiredConcurrency <= 0 ? Environment.ProcessorCount : desiredConcurrency;
            }).Invoke();
            
            // Get the list of verififcation process tasks
            var tasksList = new Func<List<Task>>(() =>
            {
                var list = new List<Task>(allowedConcurrency);
                for (var i = 0; i < allowedConcurrency; i++)
                    list.Add(Task.Run(async () => await ExecuteVerificationCycleAsync(cancellationToken), cancellationToken));
                return list;
            }).Invoke();
            
            // Last chance to throw if cancellation requested
            cancellationToken.ThrowIfCancellationRequested();
            
            // Start all verification process task concurrently and wait them all till the end
            await Task.WhenAll(tasksList);
        }
        
        private async Task ExecuteVerificationCycleAsync(CancellationToken cancellationToken)
        {
            // Get current task id, defined by the runtime
            var currentTaskId = Task.CurrentId;
            
            // Create a new services scope
            using var serviceScope = _serviceProvider.CreateScope();
            // Get a scoped serice provider instance
            var scopedServiceProvider = serviceScope.ServiceProvider;
            
            // Get the database context from the service provider
            var databaseContext = scopedServiceProvider.GetService<OvertestDatabaseContext>();
            
            // Avoid errors with scoped service provider
            if (databaseContext is null)
            {
                _logger.LogCritical("#{CurrentTaskId}: Cannot get database context from service provider!", currentTaskId);
                throw new ApplicationException();
            }

            // Run the loop till the moment, when cancellation requested by the host
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    // Start a new transaction to avoid concurrency issues
                    await using var transaction = await databaseContext.Database.BeginTransactionAsync(cancellationToken);
                    
                    // TODO: Maybe, use AnyAsync() before fetching data to ensure pending requests availability
                    
                    // Try to get information about a single pending verification request
                    var taskSolution = await databaseContext.ProgrammingTaskSolutions.AsNoTracking()
                        .Where(s => s.VerificationStatus == TaskSolution.SolutionVerificationStatus.Waiting)
                        .Where(s => _supportedCompilersOperator.SupportedCompilerIdsList.Contains(s.CompilerId))
                        .OrderBy(order => order.Created)
                        .Select(s => new TaskSolution { Id = s.Id, VerificationStatus = s.VerificationStatus })
                        .FirstOrDefaultAsync(cancellationToken);
                    
                    // No pending verification requests found, exiting
                    if (taskSolution is null)
                    {
                        // Cancel the transaction
                        await transaction.RollbackAsync(cancellationToken);
                        // Break the loop if cancellation requested
                        if (cancellationToken.IsCancellationRequested)
                            break;
                        // Make a delay to lower bandwidth and other resources usage
                        await Task.Delay(_configuration.GetValue<int>("sleep_delay"), cancellationToken);
                        continue;
                    }
                    
                    // Set verification status to "selected" to avoid concurrency
                    taskSolution.VerificationStatus = TaskSolution.SolutionVerificationStatus.Selected;
                    
                    /*
                     * We don't need to update all entry, but only specified field,
                     * so we use hidden techniques to bypass large bandwidth usage.
                     */
                    
                    databaseContext.ProgrammingTaskSolutions.Attach(taskSolution);
                    
                    // [VerificationStatus] is an concurrency stamp, so passing original value is required
                    databaseContext.Entry(taskSolution)
                        .Property(p => p.VerificationStatus)
                        .OriginalValue = TaskSolution.SolutionVerificationStatus.Waiting;
                    
                    // Inform EF Core that [VerificationStatus] property is modified
                    databaseContext.Entry(taskSolution)
                        .Property(p => p.VerificationStatus)
                        .IsModified = true;
                    
                    /*
                     * Now, we need to try saving changes and commiting the transaction.
                     * If the is an error - it is a concurrency issue, so let's ignore it.
                     */
                    try
                    {
                        // Try to save changes made to the database (in a transaction)
                        await databaseContext.SaveChangesAsync(cancellationToken);
                        // Try to commit the transaction
                        await transaction.CommitAsync(cancellationToken);
                        // Detach entity to free up the memory and to prevent any conflicts
                        databaseContext.Entry(taskSolution).State = EntityState.Detached;
                    }
                    catch (Exception) { continue; }
                    
                    _logger.LogInformation("#{CurrentTaskId}: Starting verification of '{TaskSolutionId}'...",
                        currentTaskId, taskSolution.Id);
                    
                    /*
                     * Initialize a new instance of verification process builder,
                     * build the verification process trail and start it's execution.
                     */
                    await using var verificationProcessBuilder = VerificationProcessBuilder
                        .CreateBuilder(_serviceProvider, taskSolution.Id);
                    
                    await verificationProcessBuilder
                        .PrepareVerificationProcessConfiguration()
                        .CompileTaskSolution()
                        .AddReleaseVerificationMode()
                        .PrepareVerificationResults()
                        .ExecuteAsync(cancellationToken);
                    
                    _logger.LogInformation("#{CurrentTaskId}: Verification of '{TaskSolutionId}' finished successfully!",
                        currentTaskId, taskSolution.Id);
                }
                catch (Exception ex)
                {
                    // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
                    _logger.LogError(ex, "#{CurrentTaskId}: An exception caught during the verification process: " + ex, currentTaskId);
                }
            }
        }
    }
}