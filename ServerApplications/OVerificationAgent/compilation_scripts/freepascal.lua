﻿-- Common language runtime imports
import("System.IO")
import("System.Runtime.InteropServices")
-- Importing namespaces listed below is required
-- to make the compiler plugin work correctly.
import("OTasksVerificationShared", "Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers")

-- Compiler plugin configuration section
compiler_full_path = "C:/Program Files/FreePascal/bin/i386-win32/fpc.exe" -- Full path to the compiler or command name
compiler_arguments = "source.pas -l -Px86_64 -vewnils" -- Compiler arguments (with information about source files)
compiled_executable_name = "source" -- Executable file name without extension dependant on compiler arguments

-- Function that helps compile source code into an executable program.
function Compile(sources_directory)
    -- Use default compiler execution method and get execution results
    local compilerExecutionResult = CompilationMethods:ExecuteCompiler(
        compiler_full_path,
        compiler_arguments,
        sources_directory)
    -- Use default method to form the compilation result
    return CompilationMethods:FormDefaultCompilationResult(compilerExecutionResult, 0)
end

-- Returns a full path to the main source code file. This
-- file will be created by Overtest Verification Agent.
function GetSourceCodeFileFullPath(sources_directory)
    return Path.Combine(sources_directory, compiled_executable_name .. ".pas")
end

-- This function returns process execution parameters
-- for programs, written in the programming language,
-- supported by the current compilation plugin.
function GetExecutionParams(sources_directory, arguments)
    -- Create a new instance of ExecutionParameters CLR class
    local execParams = ExecutionParameters()
    -- Set executable file name (file name extension depends on platform)
    if RuntimeInformation.IsOSPlatform(OSPlatform.Windows) then -- Platform is Windows
        execParams.ExecutableFullName = Path.Combine(sources_directory, compiled_executable_name .. ".exe")
    else -- Platform is GNU/Linux (or compatible)
        execParams.ExecutableFullName = Path.Combine(sources_directory, compiled_executable_name)
    end
    -- Set command line arguments for executable
    execParams.Arguments = arguments
    -- Return execution parameters
    return execParams
end