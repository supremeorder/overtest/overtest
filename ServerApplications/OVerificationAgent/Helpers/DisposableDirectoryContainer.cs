﻿using System;
using System.IO;
using Overtest.SharedLibraries.Common;

namespace OVerificationAgent.Helpers
{
    public class DisposableDirectoryContainer : IDisposable
    {
        public string FullPath { get; }
        
        public DisposableDirectoryContainer(string directoryFullPath)
        {
            if (string.IsNullOrWhiteSpace(directoryFullPath))
                throw new ArgumentNullException(nameof(directoryFullPath));
            
            FullPath = directoryFullPath;
            
            if (!Directory.Exists(directoryFullPath))
                Directory.CreateDirectory(FullPath);
        }

        public void Dispose()
        {
            try
            {
                if (Directory.Exists(FullPath))
                    FileSystemSharedMethods.SecureDeleteDirectory(FullPath);
            }
            catch (Exception) { /* Igore all exceptions */ }
        }

        public static DisposableDirectoryContainer CreateTempDirectory(string baseDirectory, string duplicatedDirectory = null)
        {
            if (string.IsNullOrWhiteSpace(baseDirectory))
                throw new ArgumentNullException(nameof(baseDirectory));

            if (!Directory.Exists(baseDirectory))
                throw new DirectoryNotFoundException();

            var tempDirectoryFullPath = Path.Combine(baseDirectory, Guid.NewGuid().ToString());
            
            if (!string.IsNullOrWhiteSpace(duplicatedDirectory) && Directory.Exists(duplicatedDirectory))
                FileSystemSharedMethods.SecureCopyDirectory(duplicatedDirectory, tempDirectoryFullPath);
            
            return new DisposableDirectoryContainer(tempDirectoryFullPath);
        }
    }
}