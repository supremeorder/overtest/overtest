﻿using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;

namespace OVerificationAgent.ProgramExecution
{
    public class ProgramExecutionResult
    {
        public ProgramExecutionResult()
        {
            ExitCode = -1;
            Verdict = SingleTestVerdict.VerificationFailed;

            ProgramOutput = null;
            ProgramErrorOutput = null;
            
            ResourcesUsage = new RuntimeLimitsStore
            {
                MaxRealTime = 0,
                MaxProcessorTime = 0,
                PeakWorkingSet = 0
            };
        }
        
        public int ExitCode { get; set; }
        public SingleTestVerdict Verdict { get; set; }
        
        public string ProgramOutput { get; set; }
        public string ProgramErrorOutput { get; set; }
        
        public RuntimeLimitsStore ResourcesUsage { get; set; }
    }
}