﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;

namespace OVerificationAgent.ProgramExecution
{
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    [SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global")]
    public class ProgramExecutionRequest
    {
        public string FullPath { get; set; }
        public string Arguments { get; set; }
        public string WorkingDirectory { get; set; }

        public bool RunAsEnabled { get; set; } = false;
        public string RunAsUserName { get; set; } = null;
        public string RunAsPassword { get; set; } = null;
        public string RunAsDomain { get; set; } = null;

        public string InputRedirectFileName { get; set; } = null;
        public string OutputRedirectFileName { get; set; } = null;

        public string InputRedirectFileFullPath => (InputRedirectFileName is not null
            ? Path.Combine(WorkingDirectory, InputRedirectFileName)
            : null);
        public string OutputRedirectFileFullPath => (OutputRedirectFileName is not null
            ? Path.Combine(WorkingDirectory, OutputRedirectFileName)
            : null);
        
        public RuntimeLimitsStore RuntimeLimits { get; set; }
    }
}