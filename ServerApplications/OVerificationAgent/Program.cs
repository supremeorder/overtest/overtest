﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using NLog.Fluent;
using OVerificationAgent.HostedServices;
using OVerificationAgent.SingletonServices;
using Overtest.SharedLibraries.Common;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using ILogger = NLog.ILogger;

namespace OVerificationAgent
{
    internal sealed class Program : IDisposable
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        
        // ReSharper disable once NotAccessedField.Local
        private readonly string[] _arguments;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        
        private static async Task Main(string[] args)
        {
            using var program = new Program(args);
            await program.ExecuteAsync();
        }
        
        private Program(string[] args)
        {
            // Initialize a token source
            _cancellationTokenSource = new CancellationTokenSource();
            // Store command line arguments TODO: For future usage
            _arguments = args;
            // Initialize application configuration
            _configuration = GetApplicationConfiguration();
            // Set up application logging mechanisms
            SetUpLogging(_configuration);
            // Get logger for the current class
            _logger = LogManager.GetCurrentClassLogger();
            // Set up local storage
            SetUpLocalStorage(_configuration);
        }
        
        private async Task ExecuteAsync()
        {
            _logger.Info().Message("Overtest Verification Agent is starting...")
                .Property("Start time (UTC)", DateTime.UtcNow)
                .Property("Operating system", Environment.OSVersion)
                .Property("Is AMD64 process mode", Environment.Is64BitProcess)
                .Property("CLR version", Environment.Version)
                .Write();
            
            // Don't waste time and start the host (needed to run an app)
            await StartHostAsync();
        }

        private IConfiguration GetApplicationConfiguration()
        {
            // Application's configuration file name
            const string configurationFileName = "overtest.verificationagent.config.json";
            try
            {
                // Format the path to application's configuration file and align it to the working directory
                var configurationFilePath = Path.Combine(Environment.CurrentDirectory, configurationFileName);
                // Check whether configuration file exists
                if (!File.Exists(configurationFilePath))
                    throw new FileNotFoundException("Configuration file not found!", configurationFileName);
                // Build configuration manager with the data provided in configuration file
                return new ConfigurationBuilder()
                    .SetBasePath(Environment.CurrentDirectory)
                    .AddJsonFile(configurationFilePath)
                    .Build();
            }
            catch (Exception ex)
            {
                // Inform a user that there is a problem with configuration file.
                // We should provide an inner exception for more details.
                throw new ApplicationException("There is a problem with the application's configuration file!", ex);
            }
        }

        private void SetUpLogging(IConfiguration configuration)
        {
            // Get logging configuration from configuration manager
            LogManager.Configuration = configuration.GetLoggingConfiguration();
            // Treat unhandled exceptions like fatal errors and store information about them
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                // Try to log information about exception by using the default logger
                try
                {
                    // Fatal exception logger name, used by NLog
                    const string fatalExceptionLoggerName = "unhandled_exception";
                    // Give information about the exception to logger
                    LogManager.GetLogger(fatalExceptionLoggerName)
                        .Fatal()
                        .Exception(args.ExceptionObject as Exception)
                        .Property("Exception sender", sender.GetType().FullName)
                        .Property(nameof(args.IsTerminating), args.IsTerminating)
                        .Write();
                }
                // If logger fails, fall back to the stdandard console output
                catch (Exception)
                {
                    Console.WriteLine("WARNING: An unhandled exception was thrown by the program!");
                    Console.WriteLine("We tried to wrote information about it into the logger, but failed.");
                    Console.WriteLine("See unhandled exception details below to troubleshoot an issue:");
                    Console.WriteLine((Exception) args.ExceptionObject);
                }
                // Exit an application with exit code 1 (error occurred)
                Environment.Exit(1);
            };
        }
        
        private void SetUpLocalStorage(IConfiguration configuration)
        {
            _logger.Debug("Executing method {MethodName}...", nameof(SetUpLocalStorage));
            
            // Get local storage entries list from application's configuration
            var localStorageConfiguration = configuration.GetSection("storage");
            // Create a variable for determining whether any errors occurred during the process
            var errorsFired = false;
            
            // Enumerate all local storage types and try to create directories for them
            foreach (var item in localStorageConfiguration.GetChildren())
            {
                try
                {
                    // Try to create directory for current local storage type
                    if (!Directory.Exists(item.Value))
                        Directory.CreateDirectory(item.Value);
                }
                catch (Exception ex)
                {
                    // Log information about an exception
                    _logger.Error()
                        .Message("{MethodName} An error occurred while trying to create local storage directory '{ItemKey}'!",
                            nameof(SetUpLocalStorage), item.Key)
                        .Exception(ex)
                        .Write();
                    // Error has been fired
                    errorsFired = true;
                }
            }
            // Throw an exception if errors fired during the process
            if (errorsFired)
            {
                _logger.Error("Method '{MethodName}' execution finished with errors!", nameof(SetUpLocalStorage));
                throw new ApplicationException($"Method '{nameof(SetUpLocalStorage)}' failed: can't create local storage directories!");
            }
            _logger.Debug("Method '{MethodName}' execution finished successfully!", nameof(SetUpLocalStorage));
        }

        private async Task StartHostAsync()
        {
            _logger.Debug("{MethodName}: Application host initialization started!", nameof(StartHostAsync));
            _logger.Debug("{MethodName}: Building application host...", nameof(StartHostAsync));
            
            // Create host builder and build the host with it
            using var host = CreateHostBuilder().Build();
            
            _logger.Debug("{MethodName}: Application host is ready! Starting execution...", nameof(StartHostAsync));
            
            // Run newly-created host
            await host.RunAsync();
            
            // Method that created host builder based on default settings
            IHostBuilder CreateHostBuilder() => Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    // Set up hosting environment
                    context.HostingEnvironment.ApplicationName = "Overtest Verification Agent";
                    
                    // Set up logging (using NLog provider)
                    services.AddLogging(builder =>
                    {
                        builder.ClearProviders();
                        builder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                        builder.AddNLog(_configuration.GetLoggingConfiguration());
                    });
                    
                    // Set up database context (OvertestDatabaseContext)
                    services.AddDbContext<OvertestDatabaseContext>();
                    
                    // TODO: Add other required services
                    
                    // Set up singleton operator, which handles supported compilers
                    services.AddSingleton<SupportedCompilersOperator>();
                    
                    // Set up verification data caching service - as a singleton
                    services.AddSingleton<TasksDataCachingService>();
                    
                    // Set up program execution service singleton
                    services.AddSingleton<ProgramsExecutionService>();
                    
                    // Set up the main hosted service (long-running task) that serves everything
                    services.AddHostedService<VerificationQueueHostedService>();
                }).ConfigureAppConfiguration(builder =>
                {
                    // Add existing configuration
                    builder.AddConfiguration(_configuration);
                });
        }
        
        public void Dispose()
        {
            // Cancel and dispose the cancellation token source
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
            // Shutdown the log manager gracefully
            LogManager.Shutdown();
        }
    }
}