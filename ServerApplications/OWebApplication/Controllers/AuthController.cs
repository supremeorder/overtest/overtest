﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using OWebApplication.Models.AuthController;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;

namespace OWebApplication.Controllers
{
    [AllowAnonymous, Route("/Auth")]
    public class AuthController : Controller
    {
        private readonly OvertestDatabaseContext _databaseContext;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IStringLocalizer<AuthController> _stringLocalizer;
        
        public AuthController(OvertestDatabaseContext databaseContext, UserManager<User> userManager,
            SignInManager<User> signInManager, IStringLocalizer<AuthController> stringLocalizer)
        {
            _databaseContext = databaseContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _stringLocalizer = stringLocalizer;
        }
        
        private new IActionResult View(string name, object model = null) =>
            base.View($"~/Views/AuthController/{name}.cshtml", model);

        [HttpGet, Route(nameof(Authorization))]
        public IActionResult Authorization(string returnUrl = null)
        {
            if (HttpContext.User.Identity is { IsAuthenticated: true })
                return LocalRedirect("/");
            
            return View("Authorization", new AuthorizationModel
            {
                RememberMe = true,
                ReturnUrl = returnUrl
            });
        }
        
        [HttpPost, ValidateAntiForgeryToken, Route(nameof(Authorization))]
        public async Task<IActionResult> Authorization(AuthorizationModel model, string returnUrl = null)
        {
            if (HttpContext.User.Identity is { IsAuthenticated: true })
                return LocalRedirect("/");
            
            const string viewName = "Authorization";

            if (!ModelState.IsValid)
                return View(viewName, model);

            if (returnUrl != model.ReturnUrl)
                return LocalRedirect("/");

            if (!await _databaseContext.Users.AnyAsync(u => u.Email == model.Email.ToLower() && u.IsBanned == false))
            {
                ModelState.AddModelError(nameof(model.Email), _stringLocalizer["Користувача з такими обліковими даними не знаймено, або його заблоковано."]);
                return View(viewName, model);
            }

            var signInResult = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

            if (!signInResult.Succeeded)
            {
                ModelState.AddModelError(nameof(model.Email), _stringLocalizer["Користувача з такими обліковими даними не знаймено, або його заблоковано."]);
                return View(viewName, model);
            }

            if (string.IsNullOrWhiteSpace(model.ReturnUrl))
                return LocalRedirect("/");
            
            return LocalRedirect(model.ReturnUrl);
        }

        [HttpGet, Route(nameof(Registration) + "/{joinToken?}")]
        public IActionResult Registration(string joinToken = null)
        {
            const string viewName = nameof(Registration);
            
            // Don't allow authenticated users to access this action
            if (HttpContext.User.Identity is { IsAuthenticated: true })
                return LocalRedirect("/");

            return View(viewName, new RegistrationModel
            {
                JoinToken = joinToken
            });
        }

        [HttpPost, ValidateAntiForgeryToken, Route(nameof(Registration) + "/{joinToken?}")]
        public async Task<IActionResult> Registration(RegistrationModel model, string joinToken = null)
        {
            const string viewName = nameof(Registration);
            
            // Don't allow authenticated users to access this action
            if (HttpContext.User.Identity is { IsAuthenticated: true })
                return LocalRedirect("/");

            if (!ModelState.IsValid)
                return View(viewName, model);
            
            if (joinToken != null && model.JoinToken != joinToken)
                model.JoinToken = joinToken;
            
            /*
             * Get information about invite code
             */
            
            // Get information about invite code, specified by the user
            var inviteCodeInfo = await _databaseContext.InviteCodes.AsNoTracking()
                .Where(c => c.Code == model.JoinToken)
                .Select(s => new
                {
                    s.CuratorId,
                    s.Enabled,
                    s.AutoEnableStart,
                    s.AutoEnableEnd
                }).FirstOrDefaultAsync();
            
            // If specified invite code was not found, break the process and inform the user
            if (inviteCodeInfo == null)
            {
                ModelState.AddModelError(nameof(model.JoinToken), _stringLocalizer[
                    "Вказаного вами коду запрошення не знайдено у системі. " +
                    "Зв'яжіться зі своїм куратором, щоб отримати новий код запрошення."]);
                return View(viewName, model);
            }
            
            // We need to save registration time to use it with invite code verification
            var currentUtcTime = DateTime.UtcNow;
            
            // Verify that specified invite code is active now
            var isInviteCodeActive = inviteCodeInfo.Enabled || (
                inviteCodeInfo.AutoEnableStart is not null && inviteCodeInfo.AutoEnableEnd is not null &&
                currentUtcTime >= inviteCodeInfo.AutoEnableStart && currentUtcTime <= inviteCodeInfo.AutoEnableEnd);
            
            // If invite code is expired, inform the user
            if (!isInviteCodeActive)
            {
                ModelState.AddModelError(nameof(model.JoinToken), _stringLocalizer[
                    "Вказаний вами код запрошення неактивний, або період його дії вже сплив. " +
                    "Зв'яжіться зі своїм куратором, щоб отримати новий код запрошення."]);
                return View(viewName, model);
            }
            
            // We allow only registrations with unique email addresses
            if (await _databaseContext.Users.AnyAsync(u => u.Email == model.Email))
            {
                ModelState.AddModelError(nameof(model.Email), _stringLocalizer[
                    "Користувач зі вказаною адресою електронної пошти вже існує у системі! " +
                    "Якщо ви вважаєте, що це - помилка, зв'яжіться зі своїм куратором чи адміністратором системи."]);
                return View(viewName, model);
            }
            
            // Get educational institution name from the curator's profile
            var institutionName = await _databaseContext.Users.AsNoTracking()
                .Where(u => u.Id == inviteCodeInfo.CuratorId)
                .Select(s => s.InstitutionName).FirstAsync();
            
            // Create a new [User] object for our new customer
            var user = new User
            {
                FullName = model.FullName,
                InstitutionName = institutionName,
                
                UserName = model.Email,
                Email = model.Email,

                Type = UserType.Student,
                CuratorId = inviteCodeInfo.CuratorId,

                Registered = currentUtcTime,
                LastSeen = currentUtcTime
            };
            
            // Try to create a new user with specified details and password
            var identityResult = await _userManager.CreateAsync(user, model.Password);
            
            // If we have some problems registering a new account, we need to inform the user
            if (!identityResult.Succeeded)
            {
                ModelState.AddModelError(nameof(model.Email), _stringLocalizer[
                    "На жаль, ми не змогли зареєструвати ваш обліковий запис у системі! " +
                    "Перевірте правильність заповнення реєстраційної форми. " +
                    "Також, одною з причин відмови у реєстрації є надто слабкий пароль."]);
                ModelState.AddModelError(nameof(model.FullName), identityResult.Errors.First().Description);
                return View(viewName, model);
            }
            
            // If everything is OK, try to authenticate the user
            await _signInManager.SignInAsync(user, true);
            
            // Redirect to the Overtest's homepage
            return LocalRedirect("/");
        }

        [Authorize, HttpGet, Route(nameof(LogOut))]
        public IActionResult LogOut() => View(nameof(LogOut));

        [Authorize, HttpPost, ValidateAntiForgeryToken, Route(nameof(LogOut))]
        public async Task<IActionResult> LogOut(bool force)
        {
            await _signInManager.SignOutAsync();
            return LocalRedirect("/");
        }
    }
}