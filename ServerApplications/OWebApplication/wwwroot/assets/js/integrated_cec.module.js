﻿// noinspection JSUnusedGlobalSymbols

import "../../bundle/external/acejs/ace.js"
import "../../bundle/external/acejs/ext-language_tools.js"

export function cec_initialize(element_class)
{
    // Configure base path of ace distributable package
    ace.config.set("basePath", "../../bundle/external/acejs/");
    
    // Import and initialize Ace.js extensions
    ace.require("ace/ext/language_tools");
    
    // Initialize Ace.js code editor
    let editor = ace.edit(element_class);
    
    // Set [renderer] options
    editor.setTheme("ace/theme/nord_dark");
    editor.setFontSize(16);
    editor.setShowPrintMargin(false);
    
    // Set [editor] options
    editor.setOptions({
        highlightActiveLine: true,
        highlightSelectedWord: true,
        autoScrollEditorIntoView: true,
        navigateWithinSoftTabs: false,
        enableMultiselect: true
    });
    
    // Set [session] options
    editor.session.setOptions({
        newLineMode: "windows",
        useSoftTabs: true,
        tabSize: 4,
        foldStyle: "markbeginend",
        mode: "ace/mode/text"
    });
    
    // Set [editor] [extensions] options
    editor.setOptions({
        // [ace/ext/language_tools]
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });
}

export function cec_get_code(element_class) { return ace.edit(element_class).session.getValue(); }
export function cec_set_code(element_class, new_code) { ace.edit(element_class).session.setValue(new_code); }
export function cec_set_mode(element_class, new_mode) { ace.edit(element_class).session.setMode("ace/mode/" + new_mode); }