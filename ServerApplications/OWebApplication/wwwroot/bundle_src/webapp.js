﻿// Bootstrap imports
import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"

// JQuery and plugins imports
import "../../node_modules/jquery/dist/jquery.min"
import "../../node_modules/jquery-validation/dist/jquery.validate.min"
import "../../node_modules/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min"

// Font Awesome
import "../../node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css"
import "../../node_modules/@fortawesome/fontawesome-free/css/solid.min.css"
import "../../node_modules/@fortawesome/fontawesome-free/css/brands.min.css"