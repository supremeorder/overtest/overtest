﻿using System.ComponentModel.DataAnnotations;

namespace OWebApplication.Models.AuthController
{
    public class AuthorizationModel
    {
        [Required, DataType(DataType.EmailAddress), EmailAddress]
        public string Email { get; set; }
        
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        
        public bool RememberMe { get; set; }
        
        [DataType(DataType.Text)]
        public string ReturnUrl { get; set; }
    }
}