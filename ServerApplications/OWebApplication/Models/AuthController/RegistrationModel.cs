﻿using System.ComponentModel.DataAnnotations;

namespace OWebApplication.Models.AuthController
{
    public class RegistrationModel
    {
        [Required, MinLength(3), MaxLength(255), DataType(DataType.Text)]
        public string FullName { get; set; }
        [Required, DataType(DataType.EmailAddress), EmailAddress]
        public string Email { get; set; }
        
        [Required, MinLength(8), DataType(DataType.Password)]
        public string Password { get; set; }
        [Required, DataType(DataType.Password), Compare(nameof(Password))]
        public string PasswordRepeat { get; set; }
        
        [Required, DataType(DataType.Text)]
        public string JoinToken { get; set; }
    }
}