using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MudBlazor.Services;
using OWebApplication.Operators;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;

namespace OWebApplication
{
    public class Startup
    {
        // ReSharper disable once NotAccessedField.Local
        private IConfiguration _configuration;
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            const string devAspNetCoreEnvironmentName = "Development";
            var isDevelopmentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ==
                                            devAspNetCoreEnvironmentName;
            
            // Initialize Razor pages and Blazor
            services.AddRazorPages();
            services.AddServerSideBlazor().AddHubOptions(options =>
            {
                if (isDevelopmentEnvironment)
                {
                    options.EnableDetailedErrors = true;
                }
            }).AddCircuitOptions(options =>
            {
                if (isDevelopmentEnvironment)
                    options.DetailedErrors = true;
            });
            
            // Initialize services, used for development only (if needed)
            if (isDevelopmentEnvironment)
            {
                services.AddDatabaseDeveloperPageExceptionFilter();
            }
            
            // Add localization services
            services.AddLocalization(options => { options.ResourcesPath = "Resources"; });
            services.AddControllersWithViews()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.SubFolder)
                .AddDataAnnotationsLocalization();
            
            // Add MudBlazor platform and UI dependencies
            services.AddMudServices();
            
            // Configure database and identity services
            ConfigureDataStoreAndIdentity();
            
            void ConfigureDataStoreAndIdentity()
            {
                try
                {
                    // Add primary database context
                    services.AddDbContext<OvertestDatabaseContext>(ServiceLifetime.Scoped, ServiceLifetime.Singleton);
                    services.AddDbContextFactory<OvertestDatabaseContext>();
                    
                    // Add primary identity services
                    services.AddIdentity<User, IdentityRole<Guid>>(options =>
                    {
                        // Configure username and password policies
                        options.User.RequireUniqueEmail = true;
                        options.Password.RequireDigit = true;
                        options.Password.RequiredLength = 8;
                        options.Password.RequireLowercase = true;
                        options.Password.RequireUppercase = false;
                        options.Password.RequiredUniqueChars = 3;
                        options.Password.RequireNonAlphanumeric = false;
                        
                        // Disable built-in account confirmation methods
                        options.SignIn.RequireConfirmedAccount = false;
                        options.SignIn.RequireConfirmedEmail = false;
                        options.SignIn.RequireConfirmedPhoneNumber = false;
                    }).AddEntityFrameworkStores<OvertestDatabaseContext>().AddDefaultTokenProviders();
                    
                    services.ConfigureApplicationCookie(options =>
                    {
                        options.LoginPath = "/Auth/Authorization";
                        options.LogoutPath = "/Auth/LogOut";
                        options.AccessDeniedPath = "/Security/AccessDenied";
                        options.ExpireTimeSpan = TimeSpan.FromDays(7);
                        options.Cookie.Name = "OvertestAuthCookie";
                    });

                    services.AddAuthorization(options => options.FallbackPolicy =
                        new AuthorizationPolicyBuilder()
                            .RequireAuthenticatedUser()
                            .Build());
                    
                    services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<User>>();
                    
                    // Add custom user access controller
                    services.AddSingleton<UserAccessControlOperator>();
                }
                catch (Exception innerException)
                {
                    throw new ApplicationException(
                        "Data storage & user services configuration failed! Review your Overtest configuration files and check database provider availability!",
                        innerException
                    );
                }
            }
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
            
            InitializeI18N();
            
            void InitializeI18N()
            {
                var supportedCultures = new List<CultureInfo> { new("uk-UA") };

                var options = new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture(supportedCultures.First()),
                    SupportedCultures = supportedCultures,
                    SupportedUICultures = supportedCultures
                };

                app.UseRequestLocalization(options);
            }
        }
    }
}