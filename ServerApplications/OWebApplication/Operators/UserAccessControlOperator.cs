﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;

namespace OWebApplication.Operators
{
    public class UserAccessControlOperator
    {
        public async Task<bool> HasEditPrivillegesAsync(OvertestDatabaseContext databaseContext, Guid editorUserId, Guid editedUserId)
        {
            // Allow user to edit own data
            if (editorUserId == editedUserId)
                return true;
            
            try
            {
                // Verify that editor and edited users are still alive :)
                if (await databaseContext.Users.AsNoTracking().CountAsync(u => u.Id == editorUserId || u.Id == editedUserId) != 2)
                    return false;
                
                // Get curator id of edited user
                var editedUserInfo = await databaseContext.Users.AsNoTracking()
                    .Where(u => u.Id == editedUserId)
                    .Select(s => new { s.CuratorId }).FirstAsync();
                
                // Give access edited user's curator
                if (editedUserInfo.CuratorId == editorUserId)
                    return true;
                
                // Get user type of editor
                var editorUserInfo = await databaseContext.Users.AsNoTracking()
                    .Where(u => u.Id == editorUserId)
                    .Select(s => new { s.Type }).FirstAsync();
                
                // Superuser always have the right to edit everyone
                if (editorUserInfo.Type == UserType.SuperUser)
                    return true;
                
                // You don't own this world, editor!
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> HasUserTypeAsync(OvertestDatabaseContext databaseContext, Guid userId, params UserType[] allowedUserTypes)
        {
            try
            {
                var userType = await databaseContext.Users.AsNoTracking()
                    .Where(u => u.Id == userId)
                    .Select(s => s.Type)
                    .FirstAsync();

                return Array.IndexOf(allowedUserTypes, userType) != -1;
            }
            catch
            {
                return false;
            }
        }
    }
}