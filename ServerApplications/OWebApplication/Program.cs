using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Web;
using Overtest.SharedLibraries.Common;

namespace OWebApplication
{
    public class Program
    {
        public static string CurrentVersionString => "v0.2.0-dev";
        public static DateTime CurrentVersionBuildDate => new(2021, 8, 22);
        
        private const string ConfigurationFileName = "overtest.webapplication.config.json";
        private const string DefaultLoggerName = "OVERTEST";

        private readonly IConfiguration _configuration;
        
        private Program()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(ConfigurationFileName)
                .Build();
        }
        private static async Task Main(string[] args) => await new Program().ExecuteAsync(args);

        private async Task ExecuteAsync(string[] args)
        {
            // Configure basic logging mechanisms
            ConfigureLogging();
            
            if (args.Length <= 0)
            {
                await CreateWebHostBuilder(args).Build().RunAsync();
            }
            else
            {
                throw new NotImplementedException("CLI operator not yet implemented!");
            }
        }
        
        private void ConfigureLogging()
        {
            LogManager.Configuration = _configuration.GetLoggingConfiguration();
            AppDomain.CurrentDomain.UnhandledException += (_, eventArgs) =>
                { LogManager.GetLogger(DefaultLoggerName).Fatal(eventArgs.ExceptionObject); };
        }

        private IHostBuilder CreateWebHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder =>
            {
                // Use custom startup configuration
                webBuilder.UseStartup<Startup>();
                
                /*
                 * Configure web server and its' endpoints
                 */
                webBuilder.UseKestrel(options =>
                {
                    options.AddServerHeader = true;
                    options.ConfigureEndpointDefaults(endpointOptions =>
                    {
                        endpointOptions.Protocols = HttpProtocols.Http1AndHttp2;
                        // TODO: endpointOptions.UseHttps();
                    });
                });
                webBuilder.UseUrls(_configuration.GetSection("server_endpoints").Get<string[]>());
                
                // Pass application configuration to the Kestrel
                webBuilder.ConfigureAppConfiguration((_, config) => config.AddConfiguration(_configuration));
                
                /*
                 * Configure Kestrel and web application logging
                 */
                webBuilder.ConfigureLogging(logging =>
                {
                    logging.ClearProviders(); // don't use builtin logging provider
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
                }).UseNLog();
            });
        }
    }
}