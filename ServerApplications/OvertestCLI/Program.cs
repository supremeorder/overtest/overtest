﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;
using Spectre.Console;

namespace OConfigUtility
{
    internal sealed class Program
    {
        private Program() { }
        private static T InvokeMethod<T>(Func<T> func) => func();

        private static async Task<int> Main(string[] args) => await new Program().ExecuteAsync(args);
        
        private async Task<int> ExecuteAsync(string[] args)
        {
            try
            {
                return await BuildRootCommand().InvokeAsync(args);
            }
            catch (Exception ex)
            {
                AnsiConsole.WriteException(ex);
                return -1;
            }
        }
        
        private RootCommand BuildRootCommand()
        {
            var rootCommand = new RootCommand("Use this CLI application to set up, configure and upgrade your Overtest instance.");
            rootCommand.AddCommand(GetDatabaseCommand());
            rootCommand.AddCommand(GetUsersCommands());
            rootCommand.AddCommand(GetConfigurationCommands());
            return rootCommand;
        }

        private Command GetDatabaseCommand()
        {
            var baseCommand = new Command("database-control", "This command contains all the services needed to initialize, maintain and upgrade the database of your Overtest instance.");
            baseCommand.AddAlias("database");
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("try-connect", "Try to connect to the database provider using current configuration.");
                command.Handler = CommandHandler.Create(new DatabaseCommandsOperator().TryConnectCommand);
                return command;
            }));
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("migrations", "List all, applied or pending migrations. See --help to get more information.");
                
                command.AddOption(InvokeMethod(() => new Option(new[] { "--available", "-a" /* available */ }, "") { IsRequired = false }));
                command.AddOption(InvokeMethod(() => new Option(new[] { "--applied", "-m" /* migrated */ }, "") { IsRequired = false }));
                command.AddOption(InvokeMethod(() => new Option(new[] { "--pending", "-p" /* pending */ }, "") { IsRequired = false }));
                
                command.Handler = CommandHandler.Create<bool, bool, bool>(new DatabaseCommandsOperator().MigrationsCommand);
                
                return command;
            }));
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("initialize", "Initialize or upgrade the database used by your Overtest instance.");
                command.AddAlias("migrate");
                command.Handler = CommandHandler.Create(new DatabaseCommandsOperator().InitializeDatabaseAsync);
                return command;
            }));
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("dump", "Import and export SQL used to create the database of your Overtest instance.");
                command.Handler = CommandHandler.Create<string, string, bool>(new DatabaseCommandsOperator().DumpCommand);
                
                command.AddOption(InvokeMethod(() =>
                {
                    var option = new Option<string>("--import");
                    option.Description = "Specify this option to import existing database dump in SQL format into your DBMS.";
                    option.SetDefaultValue(null);
                    option.IsRequired = false;
                    option.LegalFilePathsOnly();
                    return option;
                }));
                
                command.AddOption(InvokeMethod(() =>
                {
                    var option = new Option<string>("--export");
                    option.Description = "Specify this option to export database of your Overtest instance to the specified SQL file.";
                    option.SetDefaultValue(null);
                    option.IsRequired = false;
                    option.LegalFilePathsOnly();
                    return option;
                }));
                
                command.AddOption(InvokeMethod(() =>
                {
                    var option = new Option<bool>("--force");
                    option.Description = "Use this option to rewrite existing file specified in <--export> options.";
                    option.AddAlias("-f");
                    return option;
                }));
                
                return command;
            }));
            
            return baseCommand;
        }

        private Command GetUsersCommands()
        {
            var baseCommand = new Command("users-control", "This command can help you control user services of your Overtest instance.");
            baseCommand.AddAlias("users");
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("init-superuser", "Initialize a superuser or change it's auth credentials.");
                command.AddAlias("upd-superuser");
                command.Handler = CommandHandler.Create(new UsersCommandsOperator().InitializeSuperUser);
                return command;
            }));
            
            return baseCommand;
        }

        private Command GetConfigurationCommands()
        {
            var baseCommand = new Command("configuration-control", "Get or set current system configuration.");
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("list", "Enumerate all configuration keys stored in the database.")
                {
                    Handler = CommandHandler.Create(new ConfigurationCommandsOperator().ListCommand)
                };
                return command;
            }));
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("get", "Get value of the specified setting.");
                command.AddArgument(new Argument<string>("key"));
                command.Handler = CommandHandler.Create<string>(new ConfigurationCommandsOperator().GetCommand);
                return command;
            }));
            
            baseCommand.AddCommand(InvokeMethod(() =>
            {
                var command = new Command("set", "Set value of the specified setting.");
                command.AddArgument(new Argument<string>("key"));
                command.AddArgument(new Argument<string>("value"));
                command.Handler = CommandHandler.Create<string, string>(new ConfigurationCommandsOperator().SetCommand);
                return command;
            }));
            
            return baseCommand;
        }
    }
}