﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Spectre.Console;

namespace OConfigUtility
{
    internal sealed class ConfigurationCommandsOperator
    {
        public async Task ListCommand()
        {
            try
            {
                await using var databaseContext = new OvertestDatabaseContext();

                var keysList = await databaseContext.ConfigurationStorage
                    .OrderBy(s => s.Key)
                    .Select(s => s.Key)
                    .ToListAsync();

                foreach (var key in keysList)
                {
                    AnsiConsole.MarkupLine(key);
                }
            }
            catch (Exception)
            {
                DisplayErrorAndHalt("Can't connect to the database!");
            }
        }
        
        public async Task GetCommand(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                DisplayErrorAndHalt("Argument [bold]<key>[/] can't be [bold]null[/]!");
                return;
            }
            
            try
            {
                await using var databaseContext = new OvertestDatabaseContext();

                if (!await databaseContext.ConfigurationStorage.AnyAsync(s => s.Key == key))
                {
                    DisplayErrorAndHalt($"Key [bold]{key}[/] was not found in the configuration storage!");
                    return;
                }
                
                AnsiConsole.WriteLine(await databaseContext.ConfigurationStorage.
                    Where(s => s.Key == key)
                    .Select(s => s.Value)
                    .AsNoTracking().FirstAsync() ?? "null");
            }
            catch (Exception)
            {
                DisplayErrorAndHalt("Can't connect to the database!");
            }
        }
        
        public async Task SetCommand(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                DisplayErrorAndHalt("Argument [bold]<key>[/] can't be [bold]null[/]!");
                return;
            }
            
            if (string.IsNullOrWhiteSpace(value))
            {
                DisplayErrorAndHalt("Argument [bold]<key>[/] can't be [bold]empty[/]!");
                return;
            }

            if (value is "null" or "NULL")
                value = null;
            
            try
            {
                await using var databaseContext = new OvertestDatabaseContext();

                if (!await databaseContext.ConfigurationStorage.AnyAsync(s => s.Key == key))
                {
                    DisplayErrorAndHalt($"Key [bold]{key}[/] was not found in the configuration storage!");
                    return;
                }

                var configEntry = await databaseContext.ConfigurationStorage
                    .Where(s => s.Key == key)
                    .AsNoTracking().FirstAsync();
                
                configEntry.Value = value;
                databaseContext.ConfigurationStorage.Update(configEntry);
                await databaseContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                DisplayErrorAndHalt("Can't connect to the database!");
            }
        }

        private void DisplayErrorAndHalt(string errorText)
        {
            SharedMethods.Error(errorText);
            Environment.Exit(-1);
        }
    }
}