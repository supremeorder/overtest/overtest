﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;
using Spectre.Console;

namespace OConfigUtility
{
    internal sealed class UsersCommandsOperator
    {
        public async Task InitializeSuperUser()
        {
            SharedMethods.PrintHeader();
            
            AnsiConsole.Write(new Rule("Welcome, stranger!").LeftAligned());
            AnsiConsole.MarkupLine(
                "[bold]Welcome to the superuser's initialization (update) wizard![/] " +
                "It will guide you through the process. " +
                "Please read all the information carefully and do not stop the process when it begins.");
            AnsiConsole.WriteLine();

            if (!AnsiConsole.Confirm("Are you sure you want to continue the operation?"))
                return;
            
            AnsiConsole.WriteLine();
            
            // Creating a new disposable instance of database context
            AnsiConsole.Markup("Connecting to the database... ");
            await using var databaseContext = new OvertestDatabaseContext();
            AnsiConsole.MarkupLine("[green]Connection established![/]");
            
            // Setting up basic identity services and requesting a new UserManager<User> instance
            AnsiConsole.Markup("Setting up basic identity services... ");
            SetUpIdentity(databaseContext, out var userManager);
            AnsiConsole.MarkupLine("[green]Done![/]");
            
            // Checking whether superuser already exists in the database
            AnsiConsole.Markup("Searching for existing superuser in the database... ");
            var superUser = await databaseContext.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Type == UserType.SuperUser);
            var mustCreateNewSuperUser = superUser is null;
            AnsiConsole.MarkupLine(mustCreateNewSuperUser ? "[green]No superuser found![/]" : "[red]Superuser found![/]");
            
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Rule("Be careful during the process!").LeftAligned());
            AnsiConsole.MarkupLine(
                "Now, we need to configure a new (or existing) superuser. " +
                "Before proceeding, you must read Overtest documentation carefully! " +
                "The specified profile information will be public (except for password). " +
                "After completing this wizard, you can log into your Overtest instance using the email and password specified in it.");
            AnsiConsole.WriteLine();
            
            // Create a new user if not already exists
            superUser ??= new User
            {
                Type = UserType.SuperUser,
                IsBanned = false,
                Registered = DateTime.UtcNow,
                LastSeen = DateTime.UtcNow,
                CuratorId = null
            };
            
            // Request superuser's fullname
            superUser.FullName = AnsiConsole.Ask("Full name",
                mustCreateNewSuperUser
                    ? "System Administrator"
                    : superUser.FullName);
            
            // Request superuser's institution name
            superUser.InstitutionName = AnsiConsole.Ask("Institution name",
                mustCreateNewSuperUser
                    ? "Ivan Franko National University of Lviv"
                    : superUser.InstitutionName);
            
            // Request superuser's email address
            var superUserEmail = AnsiConsole.Prompt(new TextPrompt<string>("Email (used as username)")
                .DefaultValue(mustCreateNewSuperUser ? "admin@example.com" : superUser.Email)
                .Validate(value => IsValidEmailAddress(value.ToLower()))
            ).ToLower();
            
            // Request superuser's password
            var superUserPassword = AnsiConsole.Prompt(new TextPrompt<string>("Password:").Secret());
            
            // We can set this details only when creating a new user
            if (mustCreateNewSuperUser)
                superUser.UserName = superUser.Email = superUserEmail;
            
            /*
             * SuperUser was not found, so we must create it now.
             */
            if (mustCreateNewSuperUser)
            {
                IdentityResult identityResult = null;
                
                // Using Status() to interactively inform a user about the process
                await AnsiConsole.Status().StartAsync("Creating a new identity...", async _ =>
                {
                    identityResult = await userManager.CreateAsync(superUser, superUserPassword);
                });
                
                // Display detailed information about all errors, if applicable
                if (!identityResult.Succeeded)
                {
                    FailWithIdentityErrors(identityResult);
                    return;
                }
            }
            /*
             * SuperUser was found, so we need to update it
             * without creating a new identity entry.
             */
            else
            {
                // A signal that there are some errors in the process
                var isSuccessful = false;
                
                // Showing status strip about current operation status
                await AnsiConsole.Status().StartAsync("Updating an existing user...", async ctx =>
                {
                    // Update basic info about the user
                    databaseContext.Users.Update(superUser);
                    await databaseContext.SaveChangesAsync();
                    
                    // If email change was requested, we need to change email and username in the database
                    if (superUser.Email != superUserEmail)
                    {
                        ctx.Status("Setting up new email address...");
                        var emailChangeResult = await userManager.SetEmailAsync(superUser, superUserEmail);
                        // Display detailed information about all errors, if applicable
                        if (!emailChangeResult.Succeeded)
                        {
                            FailWithIdentityErrors(emailChangeResult);
                            return;
                        }
                        
                        ctx.Status("Setting up new username...");
                        var userNameChangeResult = await userManager.SetUserNameAsync(superUser, superUserEmail);
                        // Display detailed information about all errors, if applicable
                        if (!userNameChangeResult.Succeeded)
                        {
                            FailWithIdentityErrors(userNameChangeResult);
                            return;
                        }
                    }
                    
                    // Change superuser's password by removing old and adding a new one
                    ctx.Status("Changing superuser's password...");
                    await userManager.RemovePasswordAsync(superUser);
                    var passwordChangeResult = await userManager.AddPasswordAsync(superUser, superUserPassword);
                    if (!passwordChangeResult.Succeeded)
                    {
                        FailWithIdentityErrors(passwordChangeResult);
                        return;
                    }
                    
                    // Not required step, but including thatto be sure that all things are updated
                    ctx.Status("Updating normalized values...");
                    await userManager.UpdateNormalizedEmailAsync(superUser);
                    await userManager.UpdateNormalizedUserNameAsync(superUser);
                    
                    // Updating statuses just for fun :)
                    ctx.Status("Finishing process...");
                    isSuccessful = true;
                });
                
                // Checking if operation failed and displaying related information
                if (!isSuccessful)
                {
                    AnsiConsole.Write(new Rule("An error occured!").LeftAligned());
                    AnsiConsole.MarkupLine(
                        "[bold][red]There were some errors in the process[/][/], so the requested action partially failed. " +
                        "Please, correct all the errors listed above and start this wizard from scratch to finish the setup process!");
                    AnsiConsole.WriteLine();
                    return;
                }
            }
            
            // Informing that operation was sucessfull
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Rule("Well done!").LeftAligned());
            AnsiConsole.MarkupLine(
                "[green]Operation was successful![/] " +
                $"Superuser with email [bold]{superUserEmail}[/] and specified password was created. " +
                "Now, you can log into your Overtest instance with specified credentials.");
            AnsiConsole.WriteLine();
            
            // The program will now exit
            
            /*
             * This method is responsible for displaying
             * information about occured identity errors.
             */
            void FailWithIdentityErrors(IdentityResult identityResult)
            {
                AnsiConsole.WriteLine();
                AnsiConsole.Write(new Rule("An error occured!").LeftAligned());
                AnsiConsole.MarkupLine(
                    "[red]The operation failed[/] due to uncorrectable errors caused either by system or connectivity error or data you provided. " +
                    "See the errors list below to understand the case and troubleshoot this problem. " +
                    "Please refer to the documentation for possible resolutions.");
                AnsiConsole.WriteLine();
                
                var errorsTreeView = new Tree($"[bold]Identity errors:[/]");
                
                foreach (var error in identityResult.Errors)
                    errorsTreeView.AddNode($"{error.Code}: {error.Description}");
                
                AnsiConsole.Write(errorsTreeView);
                AnsiConsole.WriteLine();
            }
        }

        private void SetUpIdentity(OvertestDatabaseContext databaseContext, out UserManager<User> userManager)
        {
            // Set up users' store with Guid keying on the top of our database context
            IUserStore<User> userStore = new UserStore<User, IdentityRole<Guid>, OvertestDatabaseContext, Guid>(databaseContext);
            
            // Set up user manager on the top of newly created users' store
            userManager = new UserManager<User>(
                userStore, optionsAccessor: null, passwordHasher: new PasswordHasher<User>(),
                userValidators: null, passwordValidators: null, keyNormalizer: null,
                errors: null, services: null, logger: null
            );
            
            userManager.Options.User.RequireUniqueEmail = true;
            userManager.Options.Password.RequireDigit = true;
            userManager.Options.Password.RequiredLength = 8;
            userManager.Options.Password.RequireLowercase = true;
            userManager.Options.Password.RequireUppercase = false;
            userManager.Options.Password.RequiredUniqueChars = 3;
            userManager.Options.Password.RequireNonAlphanumeric = false;
        }
        
        private bool IsValidEmailAddress(string address)
        {
            try
            {
                _ = new MailAddress(address);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}