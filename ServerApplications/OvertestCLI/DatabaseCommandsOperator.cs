﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Spectre.Console;

namespace OConfigUtility
{
    internal sealed class DatabaseCommandsOperator
    {
        public async Task TryConnectCommand()
        {
            SharedMethods.PrintHeader();
            
            try
            {
                AnsiConsole.Write(new Rule("Database connection test").LeftAligned());
                AnsiConsole.MarkupLine(
                    "This command will determine whether your Overtest instance can connect to the database with a specified provider and connection string. " +
                    "Please wait till the end of the process. It can take up to a minute to run all the self-tests.");
                AnsiConsole.WriteLine();

                AnsiConsole.Markup("Creating a new instance of the database context... ");
                await using var databaseContext = new OvertestDatabaseContext();
                AnsiConsole.MarkupLine("[green]Successful![/]");

                AnsiConsole.Markup("Trying to connect to the database... ");
                var canConnect = await databaseContext.Database.CanConnectAsync();
                AnsiConsole.MarkupLine(canConnect ? "[green]Successful![/]" : "[red]Failed![/]");
                
                AnsiConsole.Markup("Fetching provider used to connect to the database... ");
                AnsiConsole.MarkupLine($"[bold][turquoise2]{databaseContext.Database.ProviderName}[/][/]");
            }
            catch (Exception ex)
            {
                AnsiConsole.WriteLine();
                AnsiConsole.WriteLine();
                
                AnsiConsole.Write(new Rule("[red]Testing process failed![/]").LeftAligned());
                AnsiConsole.MarkupLine(
                    "An error occurred during the self-testing process. " +
                    "That can be caused either by misconfiguration or connectivity issues. " +
                    "See exception dump for more information. " +
                    "We recommend you visit Overtest documentation website to learn more on how to fix this issue.");
                AnsiConsole.WriteLine();
                
                AnsiConsole.WriteException(ex);

                return;
            }
            
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Rule("[green]All tests passed![/]").LeftAligned());
            AnsiConsole.MarkupLine(
                "The testing process finished [bold][green]successfully[/][/]! " +
                "Your Overtest instance [bold]can connect[/] to the database with a specified provider and connection string. " +
                "If you are experiencing issues with database connectivity, see Overtest documentation website for more information on possible problems and their quick resolutions.");
        }
        
        public async Task MigrationsCommand(bool available, bool applied, bool pending)
        {
            SharedMethods.PrintHeader();
            
            await using var databaseContext = new OvertestDatabaseContext();
            
            if (available || (!applied && !pending))
            {
                await AnsiConsole.Status().StartAsync("Loading available migrations...", async _ =>
                {
                    var migrations = databaseContext.Database.GetMigrations().ToList();
                    PrintMigrations("All (available) migrations", migrations);
                    
                    /*
                     * We need to await completed task to make the inline action async.
                     * This caused by some specifics of Spectre.Console API. If we would
                     * use AnsiConsole.Status().Start() and not awaited StartAsync(),
                     * the database context in some cases could be destroyed before we
                     * use it here (object destroyed in outer scope warning).
                     */
                    await Task.CompletedTask;
                });
            }
            
            if (applied)
            {
                await AnsiConsole.Status().StartAsync("Loading applied migrations...", async _ =>
                {
                    var migrations = (await databaseContext.Database.GetAppliedMigrationsAsync()).ToList();
                    PrintMigrations("Applied migrations", migrations);
                });
            }
            
            if (pending)
            {
                await AnsiConsole.Status().StartAsync("Loading pending migrations...", async _ =>
                {
                    var migrations = (await databaseContext.Database.GetPendingMigrationsAsync()).ToList();
                    PrintMigrations("Pending migrations", migrations);
                });
            }
        }

        public async Task InitializeDatabaseAsync()
        {
            SharedMethods.PrintHeader();
            
            await using var databaseContext = new OvertestDatabaseContext();

            AnsiConsole.MarkupLine(
                "Welcome to the database migration companion! " +
                "This command will guide you through database initialization and migration processes. " +
                "Please be careful when upgrading an existing database to a new version. " +
                "Take all recommended steps to safely backup or export data in your Overtest instance before starting the migration process.");
            
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Rule("Migrations list").LeftAligned());
            
            var appliedMigrations = (await databaseContext.Database.GetAppliedMigrationsAsync()).ToList();
            
            AnsiConsole.MarkupLine("");
            PrintMigrations("Applied migrations", appliedMigrations);
            AnsiConsole.MarkupLine("");
            
            var pendingMigrations = (await databaseContext.Database.GetPendingMigrationsAsync()).ToList();
            
            if (!pendingMigrations.Any())
            {
                AnsiConsole.Markup("[green]There are no pending migrations![/] No further actions required.");
                return;
            }
            
            AnsiConsole.MarkupLine("");
            PrintMigrations("Pending migrations", pendingMigrations);
            AnsiConsole.MarkupLine("");

            if (!AnsiConsole.Confirm("Are you sure you want to continue the migration process?"))
                return;

            if (pendingMigrations.Any() && appliedMigrations.Any() && !AnsiConsole.Confirm(""))
                return;
            
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Rule("Migration process").LeftAligned());
            AnsiConsole.WriteLine();

            await AnsiConsole.Status().StartAsync("[yellow3_1]Migrating the database...[/] please wait.", async _ =>
            {
                try
                {
                    await databaseContext.Database.MigrateAsync();
                    AnsiConsole.MarkupLine("The migration process was successful. [green]All pending migrations were applied to the database![/]");
                }
                catch (Exception ex)
                {
                    AnsiConsole.MarkupLine("[red]A critical error occurred[/] during the migration process! More information:");
                    AnsiConsole.WriteException(ex);
                }
            });
        }

        public async Task DumpCommand(string import, string export, bool force)
        {
            SharedMethods.PrintHeader();
            
            SharedMethods.Warning("This feature is experimental! See Overtest documentation website for more information.");
            
            var isImport = !string.IsNullOrWhiteSpace(import);
            var isExport = !string.IsNullOrWhiteSpace(export);
            
            if ((isImport && isExport) || (!isImport && !isExport))
            {
                SharedMethods.Error("You must specify either [bold]<--import>[/] or [bold]<--export>[/] option!");
                return;
            }

            var outputFile = isImport ? import : export;
            
            if (isImport)
            {
                if (!File.Exists(outputFile))
                {
                    SharedMethods.Error("[bold]Specified file not found![/] See this command with [bold]<--help>[/] option to learn more.");
                    return;
                }
                
                // TODO: Import is not yet implemented!
                SharedMethods.Error("[bold]Import function is not yet implemented![/] Use your DBMS CLI interface to import the database dump.");
                return;
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            else if (isExport)
            {
                if (File.Exists(outputFile) && force == false)
                {
                    SharedMethods.Error("Specified file already exists! Use [bold]<--force>[/] or [bold]<-f>[/] option to rewrite it.");
                    return;
                }
                
                await using var databaseContext = new OvertestDatabaseContext();
                
                await AnsiConsole.Status().StartAsync("Starting the export process...", async ctx =>
                {
                    ctx.Status("Generating database creation script...");
                    var dump = databaseContext.Database.GenerateCreateScript();
                    ctx.Status("Writing script to the file...");
                    await File.WriteAllTextAsync(outputFile, dump, Encoding.UTF8);
                });
                
                // TODO: Also export data, associated with all the tables
                
                AnsiConsole.MarkupLine($"[bold][green][[SUCCESS]][/][/] Export finished!");
            }
            
            // TODO: Remove when "import" feature will be released
            await Task.CompletedTask;
        }
        
        private static void PrintMigrations(string title, IReadOnlyCollection<string> list)
        {
            var root = new Tree($"[bold]{title}:[/]");
            root.AddNodes(list.Any()
                ? list
                : new List<string> { "No migrations found!" });
            AnsiConsole.Write(root);
        }
    }
}