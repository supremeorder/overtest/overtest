﻿using Spectre.Console;

namespace OConfigUtility
{
    internal static class SharedMethods
    {
        public static void PrintHeader()
        {
            AnsiConsole.Write(new FigletText("Overtest").Color(Color.Green).Centered());
            AnsiConsole.Write(new Markup("Copyright (C) 2021, Yurii Kadirov and/or Sirkadirov. All rights reserved.").Centered());
            AnsiConsole.WriteLine();
            AnsiConsole.Write(new Markup("This software is distributed under the terms of GNU Affero GPL v3 or later.").Centered());
            AnsiConsole.WriteLine();
            AnsiConsole.WriteLine();
        }

        public static void Log(string text) => AnsiConsole.MarkupLine($"[bold][grey][[LOG]][/][/] {text}");
        public static void Warning(string text) => AnsiConsole.MarkupLine($"[bold][yellow3_1][[WARNING]][/][/] {text}");
        public static void Error(string text) => AnsiConsole.MarkupLine($"[bold][red][[ERROR]][/][/] {text}");
    }
}