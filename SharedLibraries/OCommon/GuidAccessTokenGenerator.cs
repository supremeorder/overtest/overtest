﻿using System;

namespace Overtest.SharedLibraries.Common
{
    public static class GuidAccessToken
    {
        public static string Generate() => ParseGuid(Guid.NewGuid());
        
        public static string ParseGuid(Guid guid) =>
            Convert.ToBase64String(guid.ToByteArray())
                .Replace("=","")
                .Replace("+","")
                .Replace("/","")
                .ToLower();
    }
}