﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;

#pragma warning disable CA1822
namespace Overtest.SharedLibraries.Common
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class FileSystemSharedMethods
    {
        public static void SecureRecreateDirectory(string path)
        {
            SecureDeleteDirectory(path);
            Directory.CreateDirectory(path);
        }
        
        public static void SecureDeleteDirectory(string path)
        {
            if (!Directory.Exists(path))
                return;
            
            foreach (var directoryPath in Directory.GetDirectories(path))
            {
                SecureDeleteDirectory(directoryPath);
            }
            
            foreach (var filePath in Directory.GetFiles(path))
            {
                File.SetAttributes(filePath, FileAttributes.Normal);
                File.Delete(filePath);
            }

            Directory.Delete(path);
        }
        
        public static void SecureCopyDirectory(string sourceDirectoryPath, string destinationDirectoryPath)
        {
            // Get information about the source directory
            var sourceDirectoryInfo = new DirectoryInfo(sourceDirectoryPath);
            
            // Check whether source directory exists
            if (!sourceDirectoryInfo.Exists)
                throw new DirectoryNotFoundException();
            
            // Try to create destination directory
            Directory.CreateDirectory(destinationDirectoryPath);
            
            // Copy files to a new destination
            foreach (var directoryFile in sourceDirectoryInfo.GetFiles())
            {
                directoryFile.CopyTo(Path.Combine(destinationDirectoryPath, directoryFile.Name), true);
            }
            
            // Recursive copy subdirectories
            foreach (var subDirectory in sourceDirectoryInfo.GetDirectories())
            {
                SecureCopyDirectory(subDirectory.FullName, Path.Combine(destinationDirectoryPath, subDirectory.Name));
            }
        }

        public static string ReadAllTextNormalized(string filePath)
        {
            try
            {
                var fileContent = File.ReadAllText(filePath, Encoding.UTF8);

                if (string.IsNullOrWhiteSpace(fileContent))
                    return string.Empty;

                return string.Join("\n", fileContent
                    .TrimEnd('\r', '\n', ' ')
                    .Replace("\r", "")
                    .Split('\n', StringSplitOptions.TrimEntries)
                );
            }
            catch { return string.Empty; }
        }
        
        public void ExecSecureRecreateDirectory(string path) => SecureRecreateDirectory(path);

        public void ExecSecureDeleteDirectory(string path) => SecureDeleteDirectory(path);
        
        public void ExecSecureCopyDirectory(string sourceDirectoryPath, string destinationDirectoryPath) =>
            SecureCopyDirectory(sourceDirectoryPath, destinationDirectoryPath);
        
        public string ExecReadAllTextNormalized(string filePath) => ReadAllTextNormalized(filePath);
    }
}
#pragma warning restore CA1822