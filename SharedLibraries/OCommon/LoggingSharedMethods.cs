﻿using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;

namespace Overtest.SharedLibraries.Common
{
    public static class LoggingSharedMethods
    {
        public static LoggingConfiguration GetLoggingConfiguration(this IConfiguration configuration)
        {
            var loggingConfiguration = new LoggingConfiguration();
            IConfiguration userDefinedConfiguration = configuration.GetSection("logging");
            
            var loggingLayout = new SimpleLayout("${longdate}|${logger}|${level:uppercase=true}|${message} ${all-event-properties:includeEmptyValues=true}");
            
            ConfigureConsoleTarget();
            ConfigureFilesTarget();

            return loggingConfiguration;
            
            void ConfigureConsoleTarget()
            {
                if (!userDefinedConfiguration.GetValue<bool>("console"))
                    return;
                
                var nLogColoredConsoleTarget = new ColoredConsoleTarget("console")
                {
                    Encoding = Encoding.UTF8,
                    
                    DetectConsoleAvailable = true,
                    DetectOutputRedirected = true,
                    
                    UseDefaultRowHighlightingRules = false,
                    Layout = loggingLayout,
                    AutoFlush = true
                };
                
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Trace", ConsoleOutputColor.DarkGray, ConsoleOutputColor.NoChange));
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Debug", ConsoleOutputColor.Gray, ConsoleOutputColor.NoChange));
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Info", ConsoleOutputColor.Cyan, ConsoleOutputColor.NoChange));
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Warn", ConsoleOutputColor.DarkYellow, ConsoleOutputColor.NoChange));
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Error", ConsoleOutputColor.Magenta, ConsoleOutputColor.NoChange));
                nLogColoredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Fatal", ConsoleOutputColor.White, ConsoleOutputColor.Red));
                
                loggingConfiguration.AddRule(
                    LogLevel.FromString(userDefinedConfiguration.GetValue<string>("levels:minimum")),
                    LogLevel.FromString(userDefinedConfiguration.GetValue<string>("levels:maximum")),
                    nLogColoredConsoleTarget
                );
            }

            void ConfigureFilesTarget()
            {
                var logStorageDirectory = userDefinedConfiguration.GetValue<string>("filesystem");

                try
                {
                    Directory.CreateDirectory(logStorageDirectory);
                }
                catch (Exception) { return; }
                
                var nLogFileTarget = new FileTarget
                {
                    FileName = Path.Combine(logStorageDirectory, "overtest.log"),
                    CreateDirs = true,
                    
                    KeepFileOpen = true,
                    Layout = loggingLayout,
                    Encoding = Encoding.UTF8,
                    WriteBom = false,
                    LineEnding = LineEndingMode.CRLF,
                    
                    ArchiveEvery = FileArchivePeriod.Hour,
                    ArchiveNumbering = ArchiveNumberingMode.Sequence
                };
                
                loggingConfiguration.AddRule(
                    LogLevel.FromString(userDefinedConfiguration.GetValue<string>("levels:minimum")),
                    LogLevel.FromString(userDefinedConfiguration.GetValue<string>("levels:maximum")),
                    nLogFileTarget
                );
            }
        }
    }
}