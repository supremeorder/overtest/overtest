﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using NLua;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers
{
    public class SupportedCompilerEntity : IDisposable
    {
        private readonly Lua _lua;
        
        public SupportedCompilerEntity(Guid compilerId, string sourceFullPath)
        {
            CompilerId = compilerId;
            PluginSourceFileName = sourceFullPath;

            _lua = new Lua();
            
            // Compile plugin and get an instance of the
            // class which implements ISupportedCompiler.
            InitializePluginInstance();
        }
        
        private void InitializePluginInstance()
        {
            if (CompilerId == default)
                throw new ArgumentException("You must specify appropriate compiler ID, defined in Overtest!",
                    nameof(CompilerId));
            
            if (string.IsNullOrWhiteSpace(PluginSourceFileName))
                throw new ArgumentException($"Supported compiler '{CompilerId}' plugin path cannot be null or empty!",
                    nameof(PluginSourceFileName));

            if (!File.Exists(PluginSourceFileName))
                throw new FileNotFoundException($"Supported compiler '{CompilerId}'  plugin file not found!", PluginSourceFileName);
            
            try
            {
                // Fetch the source code of the supported compiler plugin
                var sourceCode = File.ReadAllText(PluginSourceFileName, Encoding.UTF8);
                
                // Configure NLua state and load the script
                _lua.State.Encoding = Encoding.UTF8;
                _lua.LoadCLRPackage();
                _lua[nameof(CompilationMethods)] = new CompilationMethods();
                _lua.DoString(sourceCode);
                
                // Verify the structure of the supported compiler's script file
                if (_lua["Compile"] is null || _lua["GetSourceCodeFileFullPath"] is null || _lua["GetExecutionParams"] is null)
                    throw new ApplicationException($"Supported compiler '{CompilerId}': Missing required method!");
            }
            catch (UnauthorizedAccessException ex)
            {
                // This is a file load exception, not a dynamic code problem
                throw new FileLoadException($"Supported compiler '{CompilerId}' plugin file load failed!", PluginSourceFileName, ex);
            }
            catch (Exception ex)
            {
                // This exception caused by dynamic code (compilation errors, etc)
                throw new ApplicationException($"Supported compiler '{CompilerId}' plugin initialization failed!", ex);
            }
        }
        
        public Guid CompilerId { get; init; }
        public string PluginSourceFileName { get; init; }

        public CompilationResult ExecuteCompiler(string sourceDirectoryFullPath)
        {
            if (string.IsNullOrWhiteSpace(sourceDirectoryFullPath))
                throw new ArgumentNullException(nameof(sourceDirectoryFullPath));

            if (!Directory.Exists(sourceDirectoryFullPath))
                throw new DirectoryNotFoundException();

            try
            {
                var function = _lua["Compile"] as LuaFunction;
                return function?.Call(sourceDirectoryFullPath).First() as CompilationResult;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Compilation failed due to an internal compilation plugin error!", ex);
            }
        }

        public string GetSourceCodeFileFullPath(string sourceDirectoryFullPath)
        {
            if (string.IsNullOrWhiteSpace(sourceDirectoryFullPath))
                throw new ArgumentNullException(nameof(sourceDirectoryFullPath));

            if (!Directory.Exists(sourceDirectoryFullPath))
                throw new DirectoryNotFoundException();

            try
            {
                var function = _lua["GetSourceCodeFileFullPath"] as LuaFunction;
                return function?.Call(sourceDirectoryFullPath).First() as string;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Method '{nameof(GetSourceCodeFileFullPath)}' execution failed due to an internal compilation plugin error!", ex);
            }
        }
        
        public ExecutionParameters GetExecutionParameters(string sourceDirectoryFullPath, string arguments)
        {
            if (string.IsNullOrWhiteSpace(sourceDirectoryFullPath))
                throw new ArgumentNullException(nameof(sourceDirectoryFullPath));

            if (!Directory.Exists(sourceDirectoryFullPath))
                throw new DirectoryNotFoundException();

            try
            {
                var function = _lua["GetExecutionParams"] as LuaFunction;
                return function?.Call(sourceDirectoryFullPath, arguments).First() as ExecutionParameters;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Method '{nameof(GetExecutionParameters)}' execution failed due to an internal compilation plugin error!", ex);
            }
        }
        
        public void Dispose()
        {
            _lua?.Dispose();
        }
    }
}