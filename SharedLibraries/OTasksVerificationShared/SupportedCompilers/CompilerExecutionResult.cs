﻿namespace Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers
{
    public class CompilerExecutionResult
    {
        public string StandardOutput { get; set; }
        public string StandardError { get; set; }
        public int ExitCode { get; set; }
    }
}