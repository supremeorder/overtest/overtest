﻿// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Global

using System;
using System.Diagnostics;
using System.IO;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers
{
    public class CompilationMethods
    {
        public CompilerExecutionResult ExecuteCompiler(
            string fullPath, string arguments, string workingDirectory)
        {
            if (string.IsNullOrWhiteSpace(fullPath))
                throw new ArgumentNullException(nameof(fullPath));

            if (!File.Exists(fullPath))
                throw new FileNotFoundException("Compiler executable file not found!", fullPath);
            
            if (arguments is null)
                throw new ArgumentNullException(nameof(arguments));
            
            if (string.IsNullOrWhiteSpace(workingDirectory))
                throw new ArgumentNullException(nameof(workingDirectory));

            if (!Directory.Exists(workingDirectory))
                throw new DirectoryNotFoundException($"Directory '{workingDirectory}' not found!");
            
            try
            {
                var cplProc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    { 
                        FileName = fullPath, Arguments = arguments,
                        WorkingDirectory = workingDirectory,
                        UseShellExecute = false, ErrorDialog = false, 
                        RedirectStandardOutput = true, RedirectStandardError = true,
                    }
                };
                
                cplProc.Start();
                cplProc.WaitForExit();
                
                var standartOutput = cplProc.StandardOutput.ReadToEnd();
                var standartError = cplProc.StandardError.ReadToEnd();

                return new CompilerExecutionResult
                {
                    StandardOutput = standartOutput,
                    StandardError = standartError,
                    ExitCode = cplProc.ExitCode
                };
            }
            catch (Exception ex)
            {
                throw new ApplicationException("An exception caught while trying to execute compiler!", ex);
            }
        }

        public CompilationResult FormDefaultCompilationResult(
            CompilerExecutionResult compilerExecutionResult, int successfulExitCode = 0)
        {
            char[] trimCharsArray = { ' ', '\r', '\n', '\t' };
            
            compilerExecutionResult.StandardOutput = !string.IsNullOrEmpty(compilerExecutionResult.StandardOutput)
                ? compilerExecutionResult.StandardOutput.Trim(trimCharsArray)
                : null;
            compilerExecutionResult.StandardError = !string.IsNullOrEmpty(compilerExecutionResult.StandardError)
                ? compilerExecutionResult.StandardError.Trim(trimCharsArray)
                : null;
            
            var compilationResult = new CompilationResult
            {
                // Check whether compilation is successful
                IsSuccessful = (compilerExecutionResult.ExitCode == successfulExitCode),
                // Form a compiler output
                CompilationOutput = (
                    (compilerExecutionResult.StandardOutput is not null
                        ? $"{compilerExecutionResult.StandardOutput}{Environment.NewLine}{Environment.NewLine}"
                        : string.Empty) +
                    (compilerExecutionResult.StandardError ?? string.Empty)
                ).TrimEnd(trimCharsArray)
            };
            
            // Add exit code to the compilation output
            compilationResult.CompilationOutput += (
                !string.IsNullOrEmpty(compilationResult.CompilationOutput)
                    ? $"{Environment.NewLine}"
                    : string.Empty
            ) + $"**Compiler process exited with code {compilerExecutionResult.ExitCode}.**";

            return compilationResult;
        }
    }
}