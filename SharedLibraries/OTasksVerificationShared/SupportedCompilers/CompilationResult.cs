﻿namespace Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers
{
    public sealed class CompilationResult
    {
        public bool IsSuccessful { get; set; }
        public string CompilationOutput { get; set; }
    }
}