﻿namespace Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers
{
    /// ExecutionParameters struct contains information
    /// about user programs execution parameters like
    /// interpreter (or executable) full path and a list
    /// of command line arguments in a string format.
    public class ExecutionParameters
    {
        /**
         * Full path to the executable. If user's program is
         * a script, here you must specify a full path to the
         * executor or interpreter program.
         * \sa ISupportedCompiler, Arguments
         */
        public string ExecutableFullName { get; set; }
        
        /**
         * A list of command line arguments in a string format.
         * If user's program is a script, here you must specify
         * a full path to that program, other required arguments
         * to run it in a normal way, and an appended list of
         * externally-passed arguments in a string format.
         * \sa ISupportedCompiler
         */
        public string Arguments { get; set; }
    }
}