﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Overtest.SharedLibraries.ProgrammingTasksShared
{
    /// The class which stores basic information about the programming task you create.
    /// It's JSON representation must be stored in file named ".otaskforceproj" located
    /// in the root of ZIP archive containing ready to be imported programming task.
    public sealed class TaskForceProjectPropertiesStore
    {
        public TaskForceProjectPropertiesStore()
        {
            ProgrammingTaskId = Guid.NewGuid().ToString();
            ProgrammingTaskTitle = string.Empty;
            ProgrammingTaskCategory = null;
            ProgrammingTaskDifficulty = 1;
        }

        /**
         * This property must contain Guid in the string representation. Generate a new Guid
         * to created a new task, or use Guid from existing task to overwrite it.
         */
        [Required] public string ProgrammingTaskId { get; set; }

        /**
         * Specity a display name for the task you create.
         */
        [Required] public string ProgrammingTaskTitle { get; set; }

        /**
         * Task category title. Used in web application to
         * simplify navigation through the Tasks Archive.
         * If you don't want to place the task inside any
         * category, set this to null or empty.
         */
        public string ProgrammingTaskCategory { get; set; }

        /**
         * This property represents the numeric value of the task's difficulty.
         * What is difficulty? It is a rating value added to the rank of the
         * user who successfully found the solution for the task. The value of
         * this property may vary from 1 to 100.
         */
        [Required, Range(1, 100)] public int ProgrammingTaskDifficulty { get; set; }
    }
}