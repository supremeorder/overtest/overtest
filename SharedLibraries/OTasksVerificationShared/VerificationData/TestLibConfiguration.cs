﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData
{
    /// TestLibConfiguration class contains settings related to Overtest's TestLib capabilities
    /// and paths to the TestLib scripts (generators, checkers, initializers and finalizers).
    /// \sa VerificationDataConfiguration
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class TestLibConfiguration
    {
        public TestLibConfiguration()
        {
            InitializerPath = null;
            FinalizerPath = null;

            GeneratorPath = null;
            CheckerPath = null;

            DefaultInputFileName = "input.dat";
            DefaultOutputFileName = "output.dat";
        }
        
        /*!
         * Relative path to initializer script - must be present and specify real, existing path or "null" - if not used.
         * You should use "/" as the delimited for cross-platform support (Windows & GNU/Linux).
         * See official documentation for the information on how to write a initializer script.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("initializer")] public string InitializerPath { get; set; } = null;
        
        /*!
         * Relative path to finalizer script - must be present and specify real, existing path or "null" - if not used.
         * You should use "/" as the delimited for cross-platform support (Windows & GNU/Linux).
         * See official documentation for the information on how to write a finalizer script.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("finalizer")] public string FinalizerPath { get; set; } = null;
        
        /*!
         * Relative path to generator script - must be present and specify real, existing path.
         * You should use "/" as the delimited for cross-platform support (Windows & GNU/Linux).
         * See official documentation for the information on how to write a generator script.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("generator")] public string GeneratorPath { get; set; } = null;
        
        /*!
         * Relative path to checker script - must be present and specify real, existing path.
         * You should use "/" as the delimited for cross-platform support (Windows & GNU/Linux).
         * See official documentation for the information on how to write a checker script.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("checker")] public string CheckerPath { get; set; } = null;
        
        /*!
         * Relative path to the file, that will contain input data on every test.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("input_file_path")] public string DefaultInputFileName { get; set; }
        
        /*!
         * Relative path to the file, that must be created by user's program and
         * populated with execution results (program output data). If user's
         * program writes data to STDOUT, Overtest Verification Agent would
         * rewrite this file with the data present in STDOUT.
         * \sa TestLibConfiguration()
         */
        [JsonProperty("output_file_path")] public string DefaultOutputFileName { get; set; }
    }
}