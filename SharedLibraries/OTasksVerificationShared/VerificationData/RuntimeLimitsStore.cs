﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData
{
    /// RuntimeLimitsStore class stores resource limits, and is
    /// being used when configuring tasks' verification data.
    /// \sa VerificationDataConfiguration
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class RuntimeLimitsStore
    {
        public RuntimeLimitsStore()
        {
            MaxRealTime = 0;
            MaxProcessorTime = 0;
            PeakWorkingSet = 0;
        }
        
        /**
         * Specifies the hard limit of astronimic time in milliseconds
         * that can be used by process (real process execution time).
         * Must be greater than 0.
         * \sa RuntimeLimitsStore()
         */
        [JsonProperty("max_real_time")] public int MaxRealTime { get; set; }
        
        /**
         * Specifies the hard limit of processor time in milliseconds
         * that can be used by process (processor usage time).
         * Must be greater than 0.
         * \sa RuntimeLimitsStore()
         */
        [JsonProperty("max_processor_time")] public long MaxProcessorTime { get; set; }
        
        /**
         * Specifies the hard limit of peak working set usage in bytes
         * that can be used by process (in every moment of execution).
         * Must be greater than 0.
         * \sa RuntimeLimitsStore()
         */
        [JsonProperty("peak_working_set")] public long PeakWorkingSet { get; set; }
    }
}