﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData
{
    /// This class stores information about the solution for the
    /// current programming task. All fields are required to make
    /// the task verification process work correctly.
    /// \sa VerificationDataConfiguration
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class AuthorSolutionStore
    {
        public AuthorSolutionStore()
        {
            CompilerId = null;
            SourceCodePath = null;
        }
        
        //! Unique identifier of compiler, supported by current Overtest instance in string format
        [JsonProperty("compiler_id")] public string CompilerId { get; set; }
        
        //! Relative path to the directory with solution, written by the author of the task
        [JsonProperty("source_code_path")] public string SourceCodePath { get; set; }
    }
}