﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Newtonsoft.Json;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData
{
    /// VerificationDataConfiguration class stores information about the task's
    /// verification process configuration. It's JSON representation must be
    /// placed in the "verificationdata" directory of ZIP archive containing
    /// the task data, in a file called "verificationdata.config.json".
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class VerificationDataConfiguration
    {
        public VerificationDataConfiguration()
        {
            SchemaVersion = 1;
            AuthorSolution = null;
            TestLib = null;
            RuntimeLimits = null;
            ReleaseConfiguration = null;
        }
        
        //! Must be set to a version of the schema. Defaults to 1.
        [JsonProperty("$schema_version")] public int SchemaVersion { get; set; }
        
        //! See AuthorSolutionStore for more details
        [JsonProperty("author_solution")] public AuthorSolutionStore AuthorSolution { get; set; }
        
        //! See TestLibConfiguration for more details
        [JsonProperty("testlib")] public TestLibConfiguration TestLib { get; set; }
        
        //! See RuntimeLimitsStore for more details
        [JsonProperty("runtime_limits")] public RuntimeLimitsStore RuntimeLimits { get; set; }
        
        //! See ReleaseModeConfiguration for more details
        [JsonProperty("release_config")] public ReleaseModeConfiguration ReleaseConfiguration { get; set; }
        
        /**
         * Fetches programming task verification data configuration from the file,
         * deserializes it and returns VerificationDataConfiguration object (if
         * successfull) or throws an exception if failes.
         * \param fileName Full (or relative to the working directory) path to the JSOn configuration file
         * \return Verification data configuration (see class) or throws JsonException - if JSON is invalid,
         *         or Exception - in case of invalid path, access violations, etc.
         * \sa VerificationDataConfiguration
         */
        public static VerificationDataConfiguration GetConfigurationFromJson(string dataStoragePath)
        {
            try
            {
                using var streamReader = File.OpenText(Path.Combine(
                    GetVerificationDataDirectoryPath(dataStoragePath),
                    "verificationdata.config.json"));
                
                return (VerificationDataConfiguration) new JsonSerializer().Deserialize(
                    streamReader, typeof(VerificationDataConfiguration));
            }
            catch (JsonException ex) { throw new JsonException($"Testing data configuration file is invalid!", ex); }
            catch (Exception ex) { throw new FileLoadException($"Testing data configuration file not found or not accessible!", ex); }
        }
        
        public static string GetVerificationDataDirectoryPath(string dataStoragePath) =>
            Path.Combine(dataStoragePath, "verificationdata");
    }
}