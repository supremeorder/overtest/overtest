﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData
{
    /// This class stores configuration of the release verification
    /// process mode for the current programming task. Setting all
    /// the properties are required for the verififcation process
    /// to execute correctly.
    /// \sa VerificationDataConfiguration
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class ReleaseModeConfiguration
    {
        public ReleaseModeConfiguration()
        {
            RealTimeKillsLimit = 1;
            Tests = null;
        }
        
        /**
         * Property which specifies the count of allowed failed tests
         * with "real time limit reached" result. Defaults to 1. Must
         * be in range from 0 to the tests count.
         * See SingleReleaseTestInfo for more details.
         */
        [JsonProperty("real_time_kills_limit")] public int RealTimeKillsLimit { get; set; }
        
        [JsonProperty("tests")] public List<SingleTestInfo> Tests { get; set; }
    }
    
    /// This class stores information about a single release verification mode test.
    /// See ReleaseModeConfiguration for more details.
    [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
    public class SingleTestInfo
    {
        public SingleTestInfo()
        {
            ExecuteAuthorSolution = true;
            RuntimeLimits = null;
        }
        
        /**
         * Specifies the display name of the test. Can be seen by all
         * users on the task solution verififcation results page.
         */
        [JsonProperty("title")] public string Title { get; set; } = "Test";
        
        /**
         * Specifies whether author's solution execution
         * is required to verify the testing results.
         * See AuthorSolutionStore for more details.
         */
        [JsonProperty("execute_author_solution")] public bool ExecuteAuthorSolution { get; set; }
        
        /**
         * Specify runtime limits for current test only
         * See RuntimeLimitsStore for more details.
         */
        [JsonProperty("runtime_limits")] public RuntimeLimitsStore RuntimeLimits { get; set; }
    }
}