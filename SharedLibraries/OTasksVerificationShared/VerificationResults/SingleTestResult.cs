﻿using System.Diagnostics.CodeAnalysis;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
    public sealed class SingleTestResult
    {
        // ReSharper disable once UnusedMember.Global
        public SingleTestResult() {  }
        
        public SingleTestResult(string testTitle, RuntimeLimitsStore runtimeLimits)
        {
            TestTitle = testTitle;
            RuntimeLimits = runtimeLimits;
        }
        
        public string TestTitle { get; set; }
        public RuntimeLimitsStore RuntimeLimits { get; set; }
        public int ExitCode { get; set; }
        
        public long ProcessorTimeUsed { get; set; }
        public long WorkingSetUsed { get; set; }
        public long RealExecutionTimeUsed { get; set; }

        public SingleTestVerdict Verdict { get; set; } = SingleTestVerdict.VerificationFailed;
    }
}