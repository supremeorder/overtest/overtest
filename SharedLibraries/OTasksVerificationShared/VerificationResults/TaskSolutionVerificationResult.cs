﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Overtest.SharedLibraries.ProgrammingTasksShared.SupportedCompilers;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults
{
    public class TaskSolutionVerificationResult
    {
        public TaskSolutionVerificationResult()
        {
            ErrorMessage = null;
        }
        
        public CompilationResult CompilationResult { get; set; }
        public List<SingleTestResult> TestResults { get; set; }
        
        public string ErrorMessage { get; set; }

        public string ToJsonString() =>
            JsonConvert.SerializeObject(this, Formatting.None);

        public static TaskSolutionVerificationResult FromJson(string json) =>
            JsonConvert.DeserializeObject<TaskSolutionVerificationResult>(json);
    }
}