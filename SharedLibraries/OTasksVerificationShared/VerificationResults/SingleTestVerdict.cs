﻿// ReSharper disable UnusedMember.Global
namespace Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults
{
    public enum SingleTestVerdict
    {
        VerificationFailed = -100, // System reserved
        CreatedFilesLimitReached = -5,
        ProcessorTimeLimitReached = -4,
        WorkingSetLimitReached = -3,
        DiskSpaceLimitReached = -2,
        RuntimeError = -1,
        IdlingTimeout = 0,
        WrongAnswer = 1,
        WrongOutputFormat = 2,
        Successful = 100
    }
}