﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using NLua;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationResults;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.Operators
{
    public class TestLibOperator : IDisposable
    {
        private const string GeneratorFunctionName = "GenerateTest";
        private const string CheckerFunctionName = "VerifyOutput";
        private const string InitializerFunctionName = "ExecuteInitializer";
        private const string FinalizerFunctionName = "ExecuteFinalizer";
        
        private readonly string _verificationDataBasePath;

        private Lua _lua;
        
        public TestLibConfiguration Configuration { get; }

        public TestLibOperator(TestLibConfiguration testLibConfiguration, string verificationDataBasePath)
        {
            Configuration = testLibConfiguration;
            
            Configuration.GeneratorPath = NormalizeRelativePath(Configuration.GeneratorPath);
            Configuration.CheckerPath = NormalizeRelativePath(Configuration.CheckerPath);
            
            Configuration.InitializerPath = !string.IsNullOrWhiteSpace(Configuration.InitializerPath)
                ? NormalizeRelativePath(Configuration.InitializerPath) : null;
            Configuration.FinalizerPath = !string.IsNullOrWhiteSpace(Configuration.FinalizerPath)
                ? NormalizeRelativePath(Configuration.FinalizerPath) : null;
                
            _verificationDataBasePath = verificationDataBasePath;
            
            LoadLuaState();
            LoadScripts();
            
            string NormalizeRelativePath(string relativePath)
            {
                return relativePath.TrimStart('\\', '/')
                    .Replace("\\", Path.DirectorySeparatorChar.ToString())
                    .Replace("/", Path.DirectorySeparatorChar.ToString());
            }
        }
        
        private void LoadLuaState()
        {
            // Initialize a new Lua state
            _lua = new Lua();
            _lua.State.Encoding = Encoding.UTF8;
            _lua.LoadCLRPackage();
            //_lua[nameof(FileSystemSharedMethods)] = new FileSystemSharedMethods();
        }

        public bool ExecuteGenerator(int testId, string outputDirectoryPath)
        {
            try
            {
                return (bool)((LuaFunction)_lua[GeneratorFunctionName]).Call(
                    testId,
                    _verificationDataBasePath,
                    outputDirectoryPath
                ).First();
            }
            catch (Exception ex) { throw new ApplicationException($"'{nameof(ExecuteGenerator)}' method failed with exception!", ex); }
        }

        public SingleTestVerdict ExecuteChecker(int testId, string userProgramDirectoryPath, string authorProgramDirectoryPath = null)
        {
            try
            {
                return (SingleTestVerdict)(((LuaFunction)_lua[CheckerFunctionName]).Call(
                    testId,
                    _verificationDataBasePath,
                    userProgramDirectoryPath,
                    authorProgramDirectoryPath
                ).First());
            }
            catch (Exception ex) { throw new ApplicationException($"'{nameof(ExecuteChecker)}' method failed with exception!", ex); }
        }
        
        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public bool ExecuteInitializer(bool throwOnNotFound = false) => throw new NotImplementedException();
        
        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public bool ExecuteFinalizer(bool throwOnNotFound = false) => throw new NotImplementedException();

        private void LoadScripts()
        {
            if (!EnsureScriptAvailable(Configuration.GeneratorPath))
                throw new ArgumentException("TestLib initialization exception: Input generator script must be defined!");
            if (!EnsureScriptAvailable(Configuration.CheckerPath))
                throw new ArgumentException("TestLib initialization exception: Output checker script must be defined!");
            
            // Get full paths to input generator and output checker scripts
            var generatorFullPath = GetFullPathToScript(Configuration.GeneratorPath);
            var checkerFullPath = GetFullPathToScript(Configuration.CheckerPath);
            
            // If initializer script exists, get the full path to it
            var initializerFullPath = EnsureScriptAvailable(Configuration.InitializerPath)
                ? GetFullPathToScript(Configuration.InitializerPath)
                : null;
            // If finalizer script exists, get the full path to it
            var finalizerFullPath = EnsureScriptAvailable(Configuration.FinalizerPath)
                ? GetFullPathToScript(Configuration.FinalizerPath)
                : null;
            
            // Load [input generator] and [output checker] scripts into the Lua state
            _lua.DoString(File.ReadAllText(generatorFullPath, Encoding.UTF8));
            _lua.DoString(File.ReadAllText(checkerFullPath, Encoding.UTF8));
            
            // If requested, load [initializer] script to the Lua state
            if (initializerFullPath is not null)
                _lua.DoString(File.ReadAllText(initializerFullPath, Encoding.UTF8));
            // If requested, load [finalizer] script to the Lua state
            if (finalizerFullPath is not null)
                _lua.DoString(File.ReadAllText(finalizerFullPath, Encoding.UTF8));

            // Verify that [GenerateTest] and [VerifyOutput] functions are present
            if (_lua[GeneratorFunctionName] is null)
                throw new ArgumentException("[GenerateTest] is not defined!");
            if (_lua[CheckerFunctionName] is null)
                throw new ArgumentException("[VerifyOutput] is not defined!");
            
            if (initializerFullPath is not null && _lua[InitializerFunctionName] is null)
                throw new ArgumentException("[ExecuteInitializer] is not defined!");
            if (initializerFullPath is not null && _lua[FinalizerFunctionName] is null)
                throw new ArgumentException("[ExecuteFinalizer] is not defined!");
            
            bool EnsureScriptAvailable(string relativePath) => !string.IsNullOrWhiteSpace(relativePath) &&
                                                               File.Exists(GetFullPathToScript(relativePath));
            string GetFullPathToScript(string relativePath) => Path.Combine(_verificationDataBasePath, relativePath);
        }
        
        public void Dispose()
        {
            try
            {
                _lua?.Dispose();
            }
            catch { /* Ignore all exceptions */ }
        }
    }
}