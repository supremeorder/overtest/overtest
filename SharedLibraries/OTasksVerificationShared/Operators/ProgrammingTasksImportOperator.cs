﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Overtest.SharedLibraries.ProgrammingTasksShared.VerificationData;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace Overtest.SharedLibraries.ProgrammingTasksShared.Operators
{
    public class ProgrammingTasksImportOperator
    {
        public async Task<(Guid? programmingTaskId, ImportError importError)> TryImportProgrammingTaskAsync(string archiveFullPath,
            OvertestDatabaseContext databaseContext, CancellationToken? cancellationToken = null)
        {
            try
            {
                // Try to initialize a zip arichive
                using var dataStore = ZipFile.Open(archiveFullPath, ZipArchiveMode.Read, Encoding.UTF8);

                var (programmingTask, importError) =
                    await TryGetProgrammingTaskInformationFromDataStoreAsync(dataStore, cancellationToken);
                // If previous operation was not successful, pass an import error as a return value
                if (importError is not ImportError.NoErrorsFound)
                    return (null, importError);

                // Create a variable where we'll store
                // newly-created or update programming task's ID
                Guid? programmingTaskId;

                try
                {
                    // Search for required compiler in the database
                    var compilerExists = await databaseContext.Compilers.AnyAsync(
                        c => c.Id == programmingTask.RequiredCompilerId,
                        cancellationToken.GetValueOrDefault());
                    // If not exists in the database, throw an error
                    if (!compilerExists)
                        return (null, ImportError.RequiredCompilerNotFound);

                    // Create an object that we will pass to the database
                    ProgrammingTask programmingTaskDbObject;

                    var updateExistingTask = await databaseContext.ProgrammingTasks
                        .AnyAsync(t => t.Id == programmingTask.Id);

                    // If programming task with specified ID is already exists, get it's setting
                    if (updateExistingTask)
                    {
                        programmingTaskDbObject = await databaseContext.ProgrammingTasks.AsNoTracking()
                            .Where(t => t.Id == programmingTask.Id)
                            .Select(s => new ProgrammingTask
                            {
                                Enabled = s.Enabled
                            }).FirstAsync();
                    }
                    // Otherwise, create an empty object with default settings
                    else
                    {
                        // TODO: Set Enabled = false when programming tasks control ceniter is ready
                        programmingTaskDbObject = new ProgrammingTask { Enabled = true };
                    }

                    // Read an archive file into the byte array
                    var programmingTaskDataStore =
                        await File.ReadAllBytesAsync(archiveFullPath, cancellationToken.GetValueOrDefault());

                    // Populate programming task's database object with information
                    programmingTaskDbObject.Id = programmingTask.Id;
                    programmingTaskDbObject.Title = programmingTask.Title;
                    programmingTaskDbObject.Description = programmingTask.Description;
                    programmingTaskDbObject.Category = programmingTask.Category;
                    programmingTaskDbObject.Difficulty = programmingTask.Difficulty;
                    programmingTaskDbObject.RequiredCompilerId = programmingTask.RequiredCompilerId;
                    programmingTaskDbObject.DataStorage = programmingTaskDataStore;
                    programmingTaskDbObject.DataStorageHash = Guid.NewGuid();

                    // If task already exists, update it. Otherwise - add as new.
                    if (updateExistingTask)
                    {
                        // Update an existing task in the database
                        databaseContext.ProgrammingTasks.Update(programmingTaskDbObject);
                    }
                    else
                    {
                        // Try to add a new programming task to the database
                        await databaseContext.ProgrammingTasks.AddAsync(programmingTaskDbObject);
                    }

                    // Save changes made in the database
                    await databaseContext.SaveChangesAsync();

                    programmingTaskId = programmingTaskDbObject.Id;
                }
                // If any errors occurred, the database should be shamed :)
                catch (Exception)
                {
                    return (null, ImportError.DatabaseError);
                }

                // Operation was successfull
                return (programmingTaskId, ImportError.NoErrorsFound);
            }
            // Here we catch potential ZipArchive opening errors
            catch (Exception) { return (null, ImportError.ArchiveCorrupted); }
        }
        
        public async Task<(ProgrammingTask programmingTask, ImportError importError)> TryGetProgrammingTaskInformationFromDataStoreAsync(
            ZipArchive dataStore, CancellationToken? cancellationToken = null)
        {
            // Check whether operation is already cancelled
            cancellationToken?.ThrowIfCancellationRequested();
            
            try
            {
                // Try to get programming task's project properties
                var projectPropertiesStore = await GetProjectPropertiesStoreAsync(dataStore, cancellationToken);
                if (projectPropertiesStore is null)
                    return (null, ImportError.ProjectFileError);

                // Try to get programming task's description
                var programmingTaskDescription = await GetProgrammingTaskDescriptionAsync(dataStore, cancellationToken);
                if (programmingTaskDescription is null)
                    return (null, ImportError.TaskDescriptionFileError);

                var testingConfiguration =
                    await GetProgrammingTaskTestingConfigurationAsync(dataStore, cancellationToken);
                if (testingConfiguration is null)
                    return (null, ImportError.TestingConfigurationFileError);

                // Populate programming task with new data
                var programmingTaskObject = new ProgrammingTask
                {
                    Id = new Guid(projectPropertiesStore.ProgrammingTaskId),
                    Title = projectPropertiesStore.ProgrammingTaskTitle,
                    Category = string.IsNullOrWhiteSpace(projectPropertiesStore.ProgrammingTaskCategory) ? null
                        : projectPropertiesStore.ProgrammingTaskCategory.Trim(),
                    Difficulty = projectPropertiesStore.ProgrammingTaskDifficulty,
                    Description = programmingTaskDescription,

                    RequiredCompilerId = new Guid(testingConfiguration.AuthorSolution.CompilerId)
                };
                // Return a new programming task object
                return (programmingTaskObject, ImportError.NoErrorsFound);
            }
            catch (Exception)
            {
                return (null, ImportError.ArchiveCorrupted);
            }
        }

        private async Task<TaskForceProjectPropertiesStore> GetProjectPropertiesStoreAsync(ZipArchive dataStore,
            CancellationToken? cancellationToken = null)
        {
            // Check whether operation is already cancelled
            cancellationToken?.ThrowIfCancellationRequested();
            
            try
            {
                var configurationFileEntry = dataStore.GetEntry(".otaskforceproj");

                // Break the operation if file entry not found
                if (configurationFileEntry is null)
                    throw new Exception();

                using var configurationFileEntryReader = new StreamReader(configurationFileEntry.Open(), Encoding.UTF8);
                var configurationFileContents = await configurationFileEntryReader.ReadToEndAsync();

                return JsonConvert.DeserializeObject<TaskForceProjectPropertiesStore>(configurationFileContents);
            }
            catch (Exception) { return null; }
        }

        private async Task<string> GetProgrammingTaskDescriptionAsync(ZipArchive dataStore,
            CancellationToken? cancellationToken = null)
        {
            // Check whether operation is already cancelled
            cancellationToken?.ThrowIfCancellationRequested();
            
            try
            {
                var configurationFileEntry = dataStore.GetEntry("description.html");
                
                // Break the operation if file entry not found
                if (configurationFileEntry is null)
                    throw new Exception();
                
                using var descriptionFileReader = new StreamReader(configurationFileEntry.Open(), Encoding.UTF8);
                return await descriptionFileReader.ReadToEndAsync();
            }
            catch (Exception) { return null; }
        }

        private async Task<VerificationDataConfiguration> GetProgrammingTaskTestingConfigurationAsync(ZipArchive dataStore,
            CancellationToken? cancellationToken = null)
        {
            // Check whether operation is already cancelled
            cancellationToken?.ThrowIfCancellationRequested();

            try
            {
                var configurationFileEntry = dataStore.GetEntry("verificationdata/verificationdata.config.json");

                // Break the operation if file entry not found
                if (configurationFileEntry is null)
                    throw new Exception();

                using var configurationFileEntryReader = new StreamReader(configurationFileEntry.Open(), Encoding.UTF8);
                var configurationFileContents = await configurationFileEntryReader.ReadToEndAsync();

                return JsonConvert.DeserializeObject<VerificationDataConfiguration>(configurationFileContents);
            }
            catch (Exception) { return null; }
        }
        
        public enum ImportError
        {
            NoErrorsFound,
            ArchiveCorrupted,
            ProjectFileError,
            TaskDescriptionFileError,
            TestingConfigurationFileError,
            DatabaseError,
            RequiredCompilerNotFound
        }
    }
}