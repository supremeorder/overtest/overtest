﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive
{
    public class ProgrammingTask
    {
        public Guid Id { get; set; }
        public bool Enabled { get; set; }
        
        public string Title { get; set; }
        public string Category { get; set; }
        [Required, Range(1, 100)] public int Difficulty { get; set; }
        public string Description { get; set; }
        
        public Compiler RequiredCompiler { get; set; }
        public Guid RequiredCompilerId { get; set; }
        
        public byte[] DataStorage { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public Guid DataStorageHash { get; set; }
    }
}