﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global")]
    public class TaskSolution
    {
        public Guid Id { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        public TimeSpan ProcessingTime { get; set; }
        
        public SolutionVerificationType VerificationType { get; set; }
        public SolutionVerificationStatus VerificationStatus { get; set; }
        
        public string SourceCode { get; set; }
        
        public byte[] RawVerificationResult { get; set; }
        public SolutionAdjudgementType Adjudgement { get; set; }
        [Range(0, 100)] public int GivenRating { get; set; }
        
        public User Author { get; set; }
        public Guid AuthorId { get; set; }
        
        public ProgrammingTask ProgrammingTask { get; set; }
        public Guid ProgrammingTaskId { get; set; }
        
        public Compiler Compiler { get; set; }
        public Guid CompilerId { get; set; }
        
        public enum SolutionVerificationType
        {
            SyntaxMode = 1,
            DebugMode = 50,
            ReleaseMode = 100
        }
        
        public enum SolutionVerificationStatus
        {
            Waiting = 1,
            Selected = 2,
            
            AutomaticJudging = 90,
            ManualJudging = 91,
            
            Verified = 100
        }
        
        public enum SolutionAdjudgementType
        {
            NoAdjudgement = -1,
            ZeroSolution = 0,
            PartialSolution = 50,
            CompleteSolution = 100
        }
    }
}