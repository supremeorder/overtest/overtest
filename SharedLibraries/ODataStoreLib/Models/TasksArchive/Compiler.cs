﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive
{
    public class Compiler
    {
        public Guid Id { get; set; }
        
        [Required, MinLength(1), MaxLength(100)]
        public string Title { get; set; }
        
        public bool Enabled { get; set; }
        
        [Required, MinLength(1), MaxLength(255)]
        public string SyntaxHighlighter { get; set; }
    }
}