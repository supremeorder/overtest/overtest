﻿namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models
{
    public class ConfigurationStore
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}