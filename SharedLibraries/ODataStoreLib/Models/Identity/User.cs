using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity
{
    public class User : IdentityUser<Guid>
    {
        [Required, MinLength(3), MaxLength(255)]
        public string FullName { get; set; }
        
        [MinLength(0), MaxLength(255)]
        public string InstitutionName { get; set; }
        
        public UserType Type { get; set; }
        
        public bool IsBanned { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Registered { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastSeen { get; set; }
        
        public User Curator { get; set; }
        public Guid? CuratorId { get; set; }
        
        public ICollection<UserGroup> UserGroups { get; set; }
    }
    
    public enum UserType
    {
        Student = 1,
        Curator = 50,
        SuperUser = 100
    }
}