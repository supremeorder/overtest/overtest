﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity
{
    public class InviteCode
    {
        public Guid Id { get; set; }
        
        [Required] public string Code { get; set; }
        public bool Enabled { get; set; }

        public DateTime? AutoEnableStart { get; set; }
        public DateTime? AutoEnableEnd { get; set; }
        
        public Guid CuratorId { get; set; }
        public User Curator { get; set; }
    }
}