﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity
{
    public class UserGroup
    {
        public Guid Id { get; set; }
        
        [MinLength(2), MaxLength(100)]
        public string Title { get; set; }
        
        public User Curator { get; set; }
        public Guid CuratorId { get; set; }
        
        public ICollection<User> Users { get; set; }
    }
}