﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.Identity;
using Sirkadirov.Overtest.SharedLibraries.ODataStoreLib.Models.TasksArchive;

namespace Sirkadirov.Overtest.SharedLibraries.ODataStoreLib
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class OvertestDatabaseContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        /* Identity and users network */
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<InviteCode> InviteCodes { get; set; }
        
        /* Tasks archive functionality */
        public DbSet<Compiler> Compilers { get; set; }
        public DbSet<ProgrammingTask> ProgrammingTasks { get; set; }
        public DbSet<TaskSolution> ProgrammingTaskSolutions { get; set; }
        
        /* System configuration and services */
        public DbSet<ConfigurationStore> ConfigurationStorage { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            /*
             * [Users] table contains entities representing
             * real users for Overtest system, no matter
             * which privilleges they have (for example,
             * students, curators or superusers, etc).
             */
            
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(u => u.Email).IsUnique();
                
                entity.Property(u => u.FullName).IsUnicode();
                entity.Property(u => u.InstitutionName).IsUnicode().HasDefaultValue(null);
                
                entity.Property(u => u.Type).HasDefaultValue(UserType.Student).IsRequired();
                entity.Property(u => u.IsBanned).HasDefaultValue(false);
                entity.Property(u => u.CuratorId).HasDefaultValue(null);
                
                entity.Property(u => u.Registered).ValueGeneratedOnAdd();
                entity.Property(u => u.LastSeen).ValueGeneratedOnAddOrUpdate();

                entity.HasOne(u => u.Curator).WithMany()
                    .HasForeignKey(u => u.CuratorId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            
            /*
             * [User groups] table
             */

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.HasKey(g => g.Id);

                entity.HasIndex(g => new { g.CuratorId, g.Title }).IsUnique();
                entity.Property(g => g.Title).IsUnicode().IsRequired();
                entity.Property(g => g.CuratorId).IsRequired();

                entity.HasOne(g => g.Curator)
                    .WithMany()
                    .HasForeignKey(g => g.CuratorId)
                    .OnDelete(DeleteBehavior.Cascade);
                
                entity.HasMany(g => g.Users)
                    .WithMany(u => u.UserGroups)
                    .UsingEntity(e => e.ToTable("UserGroupMembers"));
            });
            
            /*
             * [Invite codes] table
             */

            modelBuilder.Entity<InviteCode>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.HasIndex(c => c.Code).IsUnique();
                entity.Property(c => c.Enabled).HasDefaultValue(false);
                entity.Property(c => c.AutoEnableStart).HasDefaultValue(null);
                entity.Property(c => c.AutoEnableEnd).HasDefaultValue(null);
                entity.Property(c => c.CuratorId).IsRequired();

                entity.HasOne(c => c.Curator).WithMany()
                    .HasForeignKey(c => c.CuratorId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            
            /*
             * [Compilers] table contains the list of all available
             * compilers and programming languages for uesers'
             * selection before they start to create solutions for
             * the programming tasks available in Tasks archive.
             */
            
            modelBuilder.Entity<Compiler>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.Property(c => c.Title).IsUnicode().IsRequired();
                entity.Property(c => c.Enabled).HasDefaultValue(true);
                entity.Property(c => c.SyntaxHighlighter).HasDefaultValue(null).IsRequired();
            });
            
            /*
             * [Programming tasks] table conatains all information of
             * programming tasks (problems) presented to users of the
             * system as the part of Tasks archive service. Users can
             * submit their solutions (source code submissions) for
             * the tasks, available in the Tasks archive, and Overtest
             * will verify them in a practical way (compiling sources
             * and executing resulting programs with specified limits
             * and I/O requirements (specified input, "ideal" output).
             */
            
            modelBuilder.Entity<ProgrammingTask>(entity =>
            {
                entity.HasKey(t => t.Id);

                entity.Property(t => t.Title).IsUnicode().HasMaxLength(255).IsRequired();
                entity.Property(t => t.Description).IsUnicode().IsRequired();
                entity.Property(t => t.Category).IsUnicode().HasMaxLength(100).HasDefaultValue(null);
                entity.Property(t => t.Difficulty).HasDefaultValue(1).IsRequired();
                
                entity.Property(t => t.Enabled).HasDefaultValue(false);
                
                entity.Property(t => t.DataStorage).IsRequired();
                entity.Property(t => t.DataStorageHash).IsRequired();

                entity.HasOne(t => t.RequiredCompiler).WithMany()
                    .HasForeignKey(t => t.RequiredCompilerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            
            /*
             * [Programming task solutions] table contains information
             * about users' submissions - solutions for programming
             * tasks presented in Tasks archive service of the system.
             */
            
            modelBuilder.Entity<TaskSolution>(entity =>
            {
                entity.HasKey(s => s.Id);
                
                entity.Property(s => s.Created).ValueGeneratedOnAdd().IsRequired();
                entity.Property(s => s.ProcessingTime).HasDefaultValue(TimeSpan.Zero);

                entity.Property(s => s.VerificationType).IsRequired();
                entity.Property(s => s.VerificationStatus)
                    .HasDefaultValue(TaskSolution.SolutionVerificationStatus.Waiting)
                    .IsConcurrencyToken();

                entity.Property(s => s.SourceCode).IsUnicode().IsRequired();

                entity.Property(s => s.RawVerificationResult).HasDefaultValue(null);
                entity.Property(s => s.Adjudgement).HasDefaultValue(TaskSolution.SolutionAdjudgementType.NoAdjudgement);
                entity.Property(s => s.GivenRating).HasDefaultValue(0);
                
                entity.HasOne(s => s.Author).WithMany()
                    .HasForeignKey(s => s.AuthorId)
                    .OnDelete(DeleteBehavior.Cascade);
                
                entity.HasOne(s => s.ProgrammingTask).WithMany()
                    .HasForeignKey(s => s.ProgrammingTaskId)
                    .OnDelete(DeleteBehavior.Cascade);
                
                entity.HasOne(s => s.Compiler).WithMany()
                    .HasForeignKey(s => s.CompilerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<ConfigurationStore>(entity =>
            {
                entity.HasKey(s => s.Key);
                entity.Property(s => s.Key).IsRequired().ValueGeneratedNever();
                entity.Property(s => s.Value).HasDefaultValue(null);
            });
            
            /*
             * Default Overtest configuration
             */
            
            modelBuilder.Entity<ConfigurationStore>().HasData(
                new ConfigurationStore { Key = "overtest.title", Value = "Overtest Website" },
                new ConfigurationStore { Key = "overtest.homelink", Value = "https://overtest.sirkadirov.com/" }
            );
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.ConfigureWarnings(builder =>
            {
                builder.Ignore(
                    //RelationalEventId.CommandCreating, RelationalEventId.CommandCreated, RelationalEventId.CommandExecuting,
                    RelationalEventId.ConnectionOpening, RelationalEventId.ConnectionOpened, RelationalEventId.ConnectionClosing, RelationalEventId.ConnectionClosed,
                    RelationalEventId.TransactionCommitting, RelationalEventId.TransactionRollingBack, RelationalEventId.TransactionStarting,
                    RelationalEventId.DataReaderDisposing, RelationalEventId.BoolWithDefaultWarning
                );
                
                builder.Log(
                    (RelationalEventId.CommandError, LogLevel.Error), (RelationalEventId.CommandExecuted, LogLevel.Debug),
                    (RelationalEventId.ConnectionError, LogLevel.Critical), (RelationalEventId.TransactionCommitted, LogLevel.Debug),
                    (RelationalEventId.TransactionDisposed, LogLevel.Trace), (RelationalEventId.TransactionError, LogLevel.Error),
                    (RelationalEventId.TransactionStarted, LogLevel.Debug), (RelationalEventId.TransactionUsed, LogLevel.Debug),
                    (RelationalEventId.TransactionRolledBack, LogLevel.Debug)
                );
            });

            var configurationFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                ?? throw new ApplicationException("Can't get base path of application during database initialization!");
            
            const string databaseConfigurationFileName = "overtest.database.config.json";
            
            var databaseConfiguration = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(configurationFilePath, databaseConfigurationFileName), false, false)
                .Build();
            
            var databaseConnectionString = databaseConfiguration.GetValue<string>("database:connection_string");
            
            // https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/projects
            // const string migrationsAssembly = "OvertestServer";
            
            switch (databaseConfiguration.GetValue<string>("database:provider").ToUpper())
            {
                case "MARIADB":
                case "MYSQL":
                    optionsBuilder.UseMySql(databaseConnectionString, ServerVersion.AutoDetect(databaseConnectionString));
                    break;
                
                default:
                    throw new DataException("You are trying to use an unknown database provider!");
            }
            
            // Allow sensitive data logging for debugging & testing development stages
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                optionsBuilder.EnableDetailedErrors();
                optionsBuilder.EnableSensitiveDataLogging();
            }
        }
    }
}